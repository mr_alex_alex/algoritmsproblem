package ru.alexch.algoritms;

import java.util.*;

// Создание всех подмножест Множества на основании последовательности Грея
public class CreaterBunchOf {

    public static void main(String[] args) {
        String[] array = new String[]{"A", "B", "C"};
        printAllSubset(array);
    }

    private static <T> void printAllSubset(T[] array) {
        for (int[] seqGrey : new CreaterGraySequense(array.length)
        ) {
            List<T> result = new ArrayList<>();
            for (int i = 0; i < seqGrey.length; i++) {
                if (seqGrey[i] == 1) {
                    result.add(array[i]);
                }
            }
            System.out.println(result);
        }

    }
}

class CreaterGraySequense implements Iterable<int[]> {

    private int[] tmp;
    private int[] seqGrey;

    public CreaterGraySequense(int n) {
        tmp = new int[n + 1];
        seqGrey = new int[n];
    }

    public void findNextGreyCode() {
        int n = seqGrey.length;
        int j = 0;
            seqGrey = Arrays.copyOf(tmp, n);
            reverse(seqGrey);
            tmp[n] = 1 - tmp[n];
            if (tmp[n] == 1) {
                j = 0;
            } else {
                for (j = 0; j < n; j++) {
                    if (tmp[j] == 1) {
                        j += 1;
                        break;
                    }
                }
            }
            if (j == n) {
             tmp =null;
             return;
            }
            tmp[j] = 1 - tmp[j];
    }

    public void reverse(int[] array) {
        for (int i = 0; i < array.length / 2; i++) {
            int tmp = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = tmp;
        }

    }

    @Override
    public Iterator<int[]> iterator() {

        class seqGreyIterator implements Iterator<int[]> {

            @Override
            public boolean hasNext() {
                return tmp != null;
            }

            @Override
            public int[] next() {
                if (hasNext()) {
                    findNextGreyCode();
                    return seqGrey;
                } else {
                    throw new NoSuchElementException();
                }
            }
        }
        return new seqGreyIterator();
    }
}

