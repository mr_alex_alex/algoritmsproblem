package ru.alexch.algoritms;

import java.io.*;
import java.util.Arrays;

//Поиск порядковой статистики
public class FinderKStatisticFromSeq {

    // Находим индекс медианного по трем точкам
    private static int findMedianElement(int[] seq, int low, int mid, int hi) {
        if (seq[low] <= seq[mid]) {
            if (seq[mid] <= seq[hi]) {
                return mid;
            }
        } else if (seq[low] <= seq[hi]) {
            return low;
        }
        return hi;
    }

    private static int partitionSeq(int[] seq, int low_index, int hi_index) {
        int support_element = seq[low_index];
        int i = low_index + 1;
        int j = hi_index;
        for (; ; ) {
            for (; i < hi_index && seq[i] < support_element; ) {
                i += 1;
            }
            for (; seq[j] > support_element; ) {
                j -= 1;
            }
            if (i >= j) {
                break;
            }
            swap(seq, i, j);
            i += 1;
            j -= 1;
        }
        swap(seq, low_index, j);
        return j;
    }

    private static void swap(int[] seq, int i, int j) {
        int tmp = seq[i];
        seq[i] = seq[j];
        seq[j] = tmp;
    }

    private static int findOrderStatistic(int[] seq, int k) {

        return findOrderStatistic(seq, 0, seq.length - 1, k);
    }

    private static int findOrderStatistic(int[] seq, int low, int hi, int k) {
        int med = findMedianElement(seq, low, low + (hi - low) / 2, hi);
        swap(seq, low, med);
        int h = partitionSeq(seq, low, hi);
        if (h == k - 1) {
            return h;
        }
        if (h < k - 1) {
            return findOrderStatistic(seq, h + 1, hi, k);
        }
        if (h > k - 1) {
            return findOrderStatistic(seq, low, h - 1, k);

        }
        return findOrderStatistic(seq, k, low, h - 1);
    }

    public static void main(String[] args) throws IOException {
//        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//        String kStr = br.readLine();
//        String seqStr = br.readLine();
//        int k = Integer.parseInt(kStr);
//        String[] seqStrS  = seqStr.split(" ");
//        int[] seq = Arrays.stream(seqStrS).mapToInt(Integer::parseInt).toArray();
//        br.close();
        int[] seq = new int[]{4, 1, 10, 9, 7, 12, 8, 2, 15};
        int k = 5;
        int result = findOrderStatistic(seq, k);
        System.out.println(seq[result]);
    }
}
