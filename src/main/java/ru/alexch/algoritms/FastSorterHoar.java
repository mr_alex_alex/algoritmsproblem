package ru.alexch.algoritms;

import java.util.Arrays;


//Быстрая сортировка разбиением Хоара
public class FastSorterHoar {
    static int count =0;

    public static int[] quickSort(int[] array) {
        quickSort(array, 0, array.length - 1);
        return array;
    }

    public static void quickSort(int[] array, int low, int hi) {
        if (low >= hi) {
            return;
        }
        int h = breakPartition(array, low, hi);
        quickSort(array, low, h - 1);
        quickSort(array, h + 1, hi);

    }

    private static int breakPartition(int[] array, int low, int hi) {
        int i = low + 1;
        int supportElem = array[low];
        int j = hi;
        for (; ; ) {
            for (; i < hi && array[i] < supportElem; ) {
                i += 1;
            }
            for (; array[j] > supportElem; ) {
                j -= 1;
            }
            if (i>j){
                break;
            }
            swap (array,i++,j--); //необходимо сначла поменять элементы а затем сузить индексы по направлению друг к ругу
            count ++;
        }
        swap(array,low, j);
        count ++;
        return j;
    }

    private static void swap(int[] array, int i, int j) {
        int tmp = array[i];
        array[i]=array[j];
        array[j]=tmp;
    }

    public static void main(String[] args) {
        int[] array = new int[]{4,1,10,9,7,12,8,2,15};
        quickSort(array);
        System.out.println(Arrays.toString(array));
        System.out.println("Количество перестановок "+count);

    }

}
