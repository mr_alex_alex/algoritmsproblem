package ru.alexch.algoritms;

import java.util.Arrays;
//Поиск строки Грея
public class FinderGraySequense {
    public void findSeq(int n) {
        int[] a = new int[n + 1];
        int j = 0;
       for(;;) {
           int[] result = Arrays.copyOf(a,n);
           reverse(result);
           String str =  Arrays.toString(result).replace(" ","").replace(",","").replace("[","").replace("]","");
            System.out.println(Arrays.toString(result)+"  "+Integer.parseInt(str,2));
            a[n] = 1 - a[n];
            if (a[n] == 1) {
                j = 0;
            } else {
                for (j = 0; j < n; j++) {
                    if (a[j] == 1) {
                        j += 1;
                        break;
                    }
                }
            }
            if (j == n) {
                return ;
            }
            a[j] = 1 - a[j];
        }
    }

    public void reverse(int[] array){
        for (int i = 0; i <array.length / 2 ; i++) {
            int tmp = array[i];
            array[i] = array[array.length -1 -i];
            array[array.length-1-i] = tmp;
        }

    }
        public static void main(String[] args) {
            FinderGraySequense finder = new FinderGraySequense();
            finder.findSeq(5);
        }
}

