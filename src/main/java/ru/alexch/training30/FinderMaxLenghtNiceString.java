package ru.alexch.training30;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class FinderMaxLenghtNiceString {
    public int countsSymb(int k, String seq) {
        int ans = 0;
        int windowsSize = 0;
        int pL = 0;
        char[] chars = seq.toCharArray();
        Integer maxCountChar = 0;
        Map<Character, Integer> cntChars = new HashMap<>();

        for (int pR = 0; pR < chars.length; pR++) {
            if (cntChars.containsKey(chars[pR])) {
                int nowCountOfChars = cntChars.get(chars[pR]);
                nowCountOfChars++;
                cntChars.put(chars[pR], nowCountOfChars);
            } else {
                cntChars.put(chars[pR], 1);
            }
            int nowCount = cntChars.get(chars[pR]);
            maxCountChar = Math.max(maxCountChar, nowCount);
            while (((pR - pL + 1) - maxCountChar) > k) {
                int nowCountOfChars = cntChars.get(chars[pL]);
                nowCountOfChars-=1;
                cntChars.put(chars[pL], nowCountOfChars);
                pL+=1;
            }
            ans = Math.max(ans, (pR - pL + 1));
        }
        return ans;
    }
    public static void main(String[] args) throws IOException {
        FinderMaxLenghtNiceString finder = new FinderMaxLenghtNiceString();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int k = Integer.parseInt(reader.readLine());
        String str;
        StringBuilder sb = new StringBuilder("");
        while ((str = reader.readLine()) != null) {
            sb.append(str);
            if (!reader.ready()) {
                break;
            }
        }
        reader.close();
        System.out.println(finder.countsSymb(k, sb.toString()));
    }
}
