package ru.alexch.training30;

import java.io.*;
import java.util.*;

import static java.lang.Integer.parseInt;

public class FinderStickerOptimV1 {
    //Diego
    long start1;
    long end1;
    long sumSpeed = 0;


    public Integer[] findSticker(int amtDiegoSticker, String strOfDiegoNomers, Integer amtOfCollector, String strOfCollectorsNomers) {
//        Integer[] numsDiego = Stream.of(strOfDiegoNomers.trim().split(" "))
//                .map(Integer::parseInt)
//                .toArray(Integer[]::new);

        String[] strOfDiegoNomersArray = strOfDiegoNomers.trim().split(" ");
        ArrayList<Integer> listDiegoNomers = new ArrayList<Integer>();
        for (String s : strOfDiegoNomersArray) {
            listDiegoNomers.add(Integer.parseInt(s));
        }

//        Integer[] numsCollectors = Stream.of(strOfCollectorsNomers.trim().split(" "))
//                .map(Integer::parseInt)
//                .toArray(Integer[]::new);

        String[] numsCollectorsArray = strOfCollectorsNomers.trim().split(" ");
        ArrayList<Integer> listNumsCollectors = new ArrayList<Integer>();

        HashMap<Integer, Integer> mapNumsCollectors = new HashMap<>();

        HashMap<Integer, Integer> mapHaveFoundNumsCollectors = new HashMap(); //найденные номера карточек и их индекс

        for (int i = 0; i < numsCollectorsArray.length; i++) {
            int nowNumFromCollector = Integer.parseInt(numsCollectorsArray[i]);
            mapNumsCollectors.put(i, nowNumFromCollector);   //для хранения индекса номера в последовательности
            // для формирования ответа
        }
        Integer[] ans = new Integer[mapNumsCollectors.size()];  //задаем размер мааиса - понадобится если последние номера из Последовательнсти Коллекторов одинаковые
        int countBefor = 0;
        HashSet<Integer> setNumsDiego = new HashSet<Integer>(listDiegoNomers);

        //отсортировали карточки Диего
        start1 = System.currentTimeMillis();
        Integer[] setNumsDiegoArray = new Integer[setNumsDiego.size()];
        setNumsDiego.toArray(setNumsDiegoArray);
        Integer[] sortedListD = quickSort(setNumsDiegoArray);
        end1 = System.currentTimeMillis();
        long resultSpeed = end1 - start1;
        sumSpeed = sumSpeed + resultSpeed;

        // определяем границы индекосов и отбрасываем лишнее
        for (Map.Entry<Integer, Integer> cardMaxNomerFromCollectors : mapNumsCollectors.entrySet()
        ) {
            Integer nowNumer = cardMaxNomerFromCollectors.getValue();
            // если карточка больше MAX номера Диего
            if (nowNumer > sortedListD[sortedListD.length - 1]) {
                countBefor = sortedListD.length;
            } else if (nowNumer <= sortedListD[0]) { //все номера Диего уже есть у Коллектора
                countBefor = 0;
                // найдем бинарным поиском последний хороший - следующий уже будет номером карты Коллектора
            } else {
                if (!mapHaveFoundNumsCollectors.containsKey(nowNumer)) {
                    int lastIndex = binsearchRight(sortedListD, nowNumer);
                    if ((lastIndex >= 0) && (sortedListD[lastIndex] != nowNumer)) {
                        countBefor = lastIndex + 1;
                    } else {
                        countBefor = 0;
                    }
                    mapHaveFoundNumsCollectors.put(nowNumer, countBefor);
                } else {   //если такую карточку у другого коллекционера мы уже Находили -ОПТМИЗАЦИЯ!!!
                    countBefor = mapHaveFoundNumsCollectors.get(nowNumer);
                }
            }
            ans[cardMaxNomerFromCollectors.getKey()] = countBefor;
        }

        return ans;
    }

    public int binsearchRight(Integer[] array, int firstSticker) {

        int L = 0;
        int R = array.length - 1;
        int m = 0;
        if (array[0] > firstSticker) {
            return -1;
        }

        while (L < R) {
            m = (R + L + 1) / 2;
            int nowNum = array[m];
            if (nowNum < firstSticker) {
                L = m;
            } else {
                R = m - 1;
            }
        }
        return L;
    }
    public  Integer[] quickSort(Integer[] array) {
        quickSort(array, 0, array.length - 1);
        return array;
    }

    public  void quickSort(Integer[] array, int lo, int hi) {
        if (lo >= hi) {
            return;
        }
        int h = breakPartition(array, lo, hi);
        quickSort(array, lo, h - 1);
        quickSort(array, h + 1, hi);
    }

    public  int breakPartition(Integer[] array, int lo, int hi) {
        int i = lo + 1;
        int supportElement = array[lo];
        int j = hi;
        for (;;) {
            for (; i < hi && array[i] < supportElement;) {
                i += 1;
            }
            for (; array[j] > supportElement;) {
                j -= 1;
            }
            if (i >= j) {
                break;
            }
            swap(array, i++, j--);
        }
        swap(array, lo, j);
        return j;
    }

    public  void swap(Integer[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    public static void main(String[] args) throws IOException {
        long timeStart = System.currentTimeMillis();
        FinderStickerOptimV1 finder = new FinderStickerOptimV1();
        //  BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Locale.setDefault(Locale.US);
        String fileName = "./input.txt";
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        int amtDiegoSticker = parseInt(reader.readLine());
        String strOfDiegoNomers;
        StringBuilder sb1 = new StringBuilder("");
        if ((strOfDiegoNomers = reader.readLine()) != null) {
            sb1.append(strOfDiegoNomers);
        } else {
            OutputStream outputStream = new FileOutputStream("output.txt");
            PrintWriter printWriter = new PrintWriter(outputStream);
            printWriter.close();
            outputStream.close();
        }
        int amtOfCollector = parseInt(reader.readLine());
        String strOfCollectorsNomers;
        StringBuilder sb2 = new StringBuilder("");
        if ((strOfCollectorsNomers = reader.readLine()) != null) {
            sb2.append(strOfCollectorsNomers);
        } else {
            OutputStream outputStream = new FileOutputStream("output.txt");
            PrintWriter printWriter = new PrintWriter(outputStream);
            printWriter.close();
            outputStream.close();
        }
        reader.close();

        Integer[] ans = null;
        if (amtDiegoSticker == 0) {
            String[] countCollectors = strOfCollectorsNomers.trim().split(" ");
            for (int i = 0; i < countCollectors.length; i++) {
                System.out.println(0);
            }
        } else if (amtOfCollector == 0) {
            String[] countDiegoCard = strOfDiegoNomers.trim().split(" ");
            System.out.println(countDiegoCard.length);
        } else {
            ans = finder.findSticker(amtDiegoSticker, strOfDiegoNomers, amtOfCollector, strOfCollectorsNomers);
            OutputStream outputStream = new FileOutputStream("output.txt");
            PrintWriter printWriter = new PrintWriter(outputStream);
            for (Integer item : ans
            ) {
                printWriter.println(item);
            }
            printWriter.close();
            outputStream.close();
        }
        long timeEnd = System.currentTimeMillis();
        System.err.println("Time(ms) = ALL " + (timeEnd - timeStart));
        System.out.println("_________________________________");
        System.err.println("Time init = " + finder.sumSpeed);

    }

}


