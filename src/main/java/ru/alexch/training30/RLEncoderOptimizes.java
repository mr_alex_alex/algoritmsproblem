package ru.alexch.training30;

public class RLEncoderOptimizes {


    public String pack(String s, int cnt) {
        StringBuilder sb = new StringBuilder(s);
        if (cnt > 1) {
            String result = String.valueOf(sb.append(Integer.toString(cnt)));
            return result;
        }
        return s;
    }

    public String encode(String s) {
        int num = 0;
        char[] chars = s.toCharArray();
        char lastsymbol = chars[0];
        int lastpas = 0;
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i <= chars.length - 1; i++) {
            if (chars[i] != lastsymbol) {
                sb.append(pack(String.valueOf(lastsymbol), i - lastpas));
                lastsymbol = chars[i];
                lastpas = i;
            }
            sb.append(pack(String.valueOf(lastsymbol), i - lastpas));
        }
        return String.valueOf(sb);
    }


}
