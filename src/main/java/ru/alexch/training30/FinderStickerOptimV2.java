package ru.alexch.training30;

import java.io.*;
import java.util.*;
import java.util.stream.Stream;

import static java.lang.Integer.parseInt;

public class FinderStickerOptimV2 {
    //Diego
//    long start1;
//    long end1;
//    long sumSpeed = 0;
//    long Mbefore;
//    long Mafter;


    public int[] findSticker(int amtDiegoSticker, String strOfDiegoNomers, Integer amtOfCollector, String strOfCollectorsNomers) {
     //   Mbefore = mem();
        HashMap<Integer, Integer> mapNumsCollectors = new HashMap<>();
        int[] setNumsDiego = Stream.of(strOfDiegoNomers.trim().split(" "))
                .map(Integer::parseInt).mapToInt(i->i)
                .distinct()
                .toArray();

        int[] numsCollectorsArray = Stream.of(strOfCollectorsNomers.trim().split(" "))
                .map(Integer::parseInt).mapToInt(i->i)
                .toArray();

        HashMap<Integer, Integer> mapHaveFoundNumsCollectors = new HashMap(); //найденные номера карточек и их индекс
      //  Mafter = mem();
      //  long Mrezult =Mafter-Mbefore;
        int countBefor = 0;
     //   start1 = System.currentTimeMillis();
            Arrays.sort(setNumsDiego);
        //quickSort(setNumsDiego);
     //   end1 = System.currentTimeMillis();
      //  long resultSpeed = end1 - start1;
     //   sumSpeed = sumSpeed + resultSpeed;

        for (int i = 0; i < numsCollectorsArray.length; i++) {
            mapNumsCollectors.put(i, numsCollectorsArray[i]);   //для хранения индекса номера в последовательности
            // для формирования ответа
        }
        int[] ans = new int[mapNumsCollectors.size()];  //задаем размер массива - понадобится если последние номера из Последовательнсти Коллекторов одинаковые


        // определяем границы индекосов и отбрасываем лишнее
        for (Map.Entry<Integer, Integer> cardMaxNomerFromCollectors : mapNumsCollectors.entrySet()
        ) {
            int nowNumer = cardMaxNomerFromCollectors.getValue();
            // если карточка больше MAX номера Диего
            if (nowNumer > setNumsDiego[setNumsDiego.length - 1]) {
                countBefor = setNumsDiego.length;
            } else if (nowNumer <= setNumsDiego[0]) { //все номера Диего уже есть у Коллектора
                countBefor = 0;
                // найдем бинарным поиском последний хороший - следующий уже будет номером карты Коллектора
            } else {
                if (!mapHaveFoundNumsCollectors.containsKey(nowNumer)) {
                    int lastIndex = binsearchRight(setNumsDiego, nowNumer);
                    if ((lastIndex >= 0) && (setNumsDiego[lastIndex] != nowNumer)) {
                        countBefor = lastIndex + 1;
                    } else {
                        countBefor = 0;
                    }
                    mapHaveFoundNumsCollectors.put(nowNumer, countBefor);
                } else {   //если такую карточку у другого коллекционера мы уже Находили -ОПТМИЗАЦИЯ!!!
                    countBefor = mapHaveFoundNumsCollectors.get(nowNumer);
                }
            }
            ans[cardMaxNomerFromCollectors.getKey()] = countBefor;
        }

        return ans;
    }

    public static long mem() {
        Runtime runtime = Runtime.getRuntime();
        return runtime.totalMemory() - runtime.freeMemory();
    }
    public int binsearchRight(int[] array, int firstSticker) {

        int L = 0;
        int R = array.length - 1;
        int m = 0;
        if (array[0] > firstSticker) {
            return -1;
        }

        while (L < R) {
            m = (R + L + 1) / 2;
            int nowNum = array[m];
            if (nowNum < firstSticker) {
                L = m;
            } else {
                R = m - 1;
            }
        }
        return L;
    }

    public static void quickSort(int[] array) {
        if (array.length ==0){
            return;
        }
        quickSort(array, 0, array.length - 1);
    }

    public static void quickSort(int[] array, int from, int to) {

        if (from <= to) {
            return;
        }
        int h = breakPartition(array, from, to);
        quickSort(array, from, h - 1);
        quickSort(array, h , to);
    }

//    public static int breakPartition(int[] array, int lo, int hi) {
//        int i = lo + 1;
//        int supportElement = array[lo +(hi-lo)/2];
//        int j = hi;
//        for (; ; ) {
//            for (; i < hi && array[i] < supportElement; ) {
//                i += 1;
//            }
//            for (; array[j] > supportElement; ) {
//                j -= 1;
//            }
//            if (i >= j) {
//                break;
//            }
//            swap(array, i++, j--);
//        }
//        swap(array, lo, j);
//        return j;
//    }

    public static int breakPartition(int[] array, int from, int to) {
        int rightIndex = to;
        int leftIndex = from;
        int pivot = array[from + (to-from)/2];
         while (leftIndex <=rightIndex){

             while (array[leftIndex]< pivot){
                 leftIndex++;
             }
             while (array[rightIndex]> pivot){
                 rightIndex--;
             }
             if (leftIndex<=rightIndex){
                 swap(array,rightIndex, leftIndex);
                 leftIndex++;
                 rightIndex--;
             }
         }
         return leftIndex;
    }
    public static void swap(int[] array, int index1, int index2) {
        int temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }

    public static void main(String[] args) throws IOException {
   //     long timeStart = System.currentTimeMillis();
        FinderStickerOptimV2 finder = new FinderStickerOptimV2();
        Locale.setDefault(Locale.US);
        String fileName = "./input.txt";
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        int amtDiegoSticker = parseInt(reader.readLine());
        String strOfDiegoNomers;
        StringBuilder sb1 = new StringBuilder("");
        if ((strOfDiegoNomers = reader.readLine()) != null) {
            sb1.append(strOfDiegoNomers);
        } else {
            OutputStream outputStream = new FileOutputStream("output.txt");
            PrintWriter printWriter = new PrintWriter(outputStream);
            printWriter.close();
            outputStream.close();
        }
        int amtOfCollector = parseInt(reader.readLine());
        String strOfCollectorsNomers;
        StringBuilder sb2 = new StringBuilder("");
        if ((strOfCollectorsNomers = reader.readLine()) != null) {
            sb2.append(strOfCollectorsNomers);
        } else {
            OutputStream outputStream = new FileOutputStream("output.txt");
            PrintWriter printWriter = new PrintWriter(outputStream);
            printWriter.close();
            outputStream.close();
        }
        reader.close();

        int[] ans = null;
        if (amtDiegoSticker == 0) {
            String[] countCollectors = strOfCollectorsNomers.trim().split(" ");
            for (int i = 0; i < countCollectors.length; i++) {
                System.out.println(0);
            }
        } else if (amtOfCollector == 0) {
            String[] countDiegoCard = strOfDiegoNomers.trim().split(" ");
            System.out.println(countDiegoCard.length);
        } else {
            ans = finder.findSticker(amtDiegoSticker, strOfDiegoNomers, amtOfCollector, strOfCollectorsNomers);
            OutputStream outputStream = new FileOutputStream("output.txt");
            PrintWriter printWriter = new PrintWriter(outputStream);
            for (Integer item : ans
            ) {
                printWriter.println(item);
            }
            printWriter.close();
            outputStream.close();
        }
//        long timeEnd = System.currentTimeMillis();
//        System.err.println("Time(ms) = ALL " + (timeEnd - timeStart));
//        System.out.println("_________________________________");
//        System.err.println("Time init = " + finder.sumSpeed);
//        System.err.println("Mem init = " + (finder.Mafter-finder.Mbefore));
    }
}
