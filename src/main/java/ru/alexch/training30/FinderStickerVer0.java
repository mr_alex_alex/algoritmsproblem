package ru.alexch.training30;

import java.io.*;
import java.util.*;
import java.util.stream.Stream;

import static java.lang.Integer.parseInt;

//Diego
public class FinderStickerVer0 {

    public ArrayList<Integer> findSticker(int amtDiegoSticker, String strOfDiegoNomers, Integer amtOfCollector, String strOfCollectorsNomers) {
//        Integer[] numsDiego = Stream.of(strOfDiegoNomers.trim().split(" "))
//                .map(Integer::parseInt)
//                .toArray(Integer[]::new);

        String[] strOfDiegoNomersArray = strOfDiegoNomers.trim().split(" ");
        ArrayList<Integer> listDiegoNomers = new ArrayList<Integer>();
        for (int i = 0; i < strOfDiegoNomersArray.length; i++) {
            listDiegoNomers.add(Integer.parseInt(strOfDiegoNomersArray[i]));
        }

//        Integer[] numsCollectors = Stream.of(strOfCollectorsNomers.trim().split(" "))
//                .map(Integer::parseInt)
//                .toArray(Integer[]::new);
        String[] numsCollectorsArray = strOfCollectorsNomers.trim().split(" ");
        ArrayList<Integer> listNumsCollectors = new ArrayList<Integer>();
        for (int i = 0; i < numsCollectorsArray.length; i++) {
            listNumsCollectors.add(Integer.parseInt(numsCollectorsArray[i]));
        }

        ArrayList<Integer> ans = new ArrayList<Integer>();

        int countBefor = 0;
        //ToDo оптимизация с учетом повтора колеекций у коллекционера
        for (int i = 0; i < listNumsCollectors.size(); i++) {
            ArrayList<Integer> listD = new ArrayList<Integer>(listDiegoNomers);
            listD.add(listNumsCollectors.get(i)); //закинем первый номер карточек Коллекционера в список
            TreeSet<Integer> setNumsDiego = new TreeSet<Integer>(listD);
            if (setNumsDiego.size() > 1) {
                int lastNumIndex = binsearch(setNumsDiego, listNumsCollectors.get(i));
                countBefor = lastNumIndex > 0 ? (lastNumIndex - 1) + 1 : 0;
            } else {
                countBefor = findStickerLess(listD.get(0), listNumsCollectors.get(i));
            }
            ans.add(countBefor);
            countBefor = 0;
            setNumsDiego = null;
            listD = null;

        }
        return ans;
    }

    public int binsearch(TreeSet<Integer> sortedList, int firstSticker) {
        ArrayList<Integer> list = new ArrayList<Integer>(sortedList);
        int L = 0;
        int R = list.size() - 1;
        int m = 0;
        if (firstSticker <= 0) {
            m = -1;
        }

        while (L <= R) {
            m = L + (R - L) / 2;
            int nowNum = list.get(m);
            if (firstSticker < list.get(m)) {
                R = m - 1;
            } else if (firstSticker > list.get(m)) {
                L = m + 1;
            } else {
                return m;
            }
        }
        return m;
    }

    public int findStickerLess(int n, int m) {
        int ans = 0;
        if (n < m) {
            ans = 1;
        }
        return ans;
    }

    public static void main(String[] args) throws IOException {
        long timeStart = System.currentTimeMillis();
        FinderStickerVer0 finder = new FinderStickerVer0();
        //  BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Locale.setDefault(Locale.US);
        String fileName = "./Testinput.txt";
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        int amtDiegoSticker = parseInt(reader.readLine());
        String strOfDiegoNomers;
        StringBuilder sb1 = new StringBuilder("");
        if ((strOfDiegoNomers = reader.readLine()) != null) {
            sb1.append(strOfDiegoNomers);
        } else {
            OutputStream outputStream = new FileOutputStream("output.txt");
            PrintWriter printWriter = new PrintWriter(outputStream);
            printWriter.close();
            outputStream.close();
        }
        int amtOfCollector = parseInt(reader.readLine());
        String strOfCollectorsNomers;
        StringBuilder sb2 = new StringBuilder("");
        if ((strOfCollectorsNomers = reader.readLine()) != null) {
            sb2.append(strOfCollectorsNomers);
        } else {
            OutputStream outputStream = new FileOutputStream("output.txt");
            PrintWriter printWriter = new PrintWriter(outputStream);
            printWriter.close();
            outputStream.close();
        }
        reader.close();

        ArrayList<Integer> ans = null;
        if (amtDiegoSticker == 0) {
            String[] countCollectors = strOfCollectorsNomers.trim().split(" ");
            for (int i = 0; i < countCollectors.length; i++) {
                System.out.println(0);
            }
        } else if (amtOfCollector == 0) {
            String[] countDiegoCard = strOfDiegoNomers.trim().split(" ");
            System.out.println(countDiegoCard.length);
        } else {
            ans = finder.findSticker(amtDiegoSticker, strOfDiegoNomers, amtOfCollector, strOfCollectorsNomers);
            Object[] arrAns = ans.toArray();

            OutputStream outputStream = new FileOutputStream("output.txt");
            PrintWriter printWriter = new PrintWriter(outputStream);
            for (Integer item : ans
            ) {
                printWriter.println(item);
            }
            printWriter.close();
            outputStream.close();
        }
        long timeEnd = System.currentTimeMillis();
        System.err.println("Time(ms) = " + (timeEnd - timeStart));
    }
}
