package ru.alexch.training30;

import javax.swing.tree.TreeNode;
import java.io.*;
import java.util.*;
import java.util.stream.Stream;

import static java.lang.Integer.parseInt;

//Diego
public class FinderSticker {
    long timeStartBin;
    long timeStopBin;
    long speedBin;
    long start1;
    long end1;
    long sumSpeed=0;
    public ArrayList<Integer> findSticker(int amtDiegoSticker, String strOfDiegoNomers, Integer amtOfCollector, String strOfCollectorsNomers) {
//        Integer[] numsDiego = Stream.of(strOfDiegoNomers.trim().split(" "))
//                .map(Integer::parseInt)
//                .toArray(Integer[]::new);

        String[] strOfDiegoNomersArray = strOfDiegoNomers.trim().split(" ");
        ArrayList<Integer> listDiegoNomers = new ArrayList<Integer>();
        for (int i = 0; i < strOfDiegoNomersArray.length; i++) {
            listDiegoNomers.add(Integer.parseInt(strOfDiegoNomersArray[i]));
        }

//        Integer[] numsCollectors = Stream.of(strOfCollectorsNomers.trim().split(" "))
//                .map(Integer::parseInt)
//                .toArray(Integer[]::new);
        String[] numsCollectorsArray = strOfCollectorsNomers.trim().split(" ");
        ArrayList<Integer> listNumsCollectors = new ArrayList<Integer>();
        for (int i = 0; i < numsCollectorsArray.length; i++) {
            listNumsCollectors.add(Integer.parseInt(numsCollectorsArray[i]));
        }

        Map<Integer, Integer> resultForCollectors = new HashMap<Integer, Integer>();
        ArrayList<Integer> ans = new ArrayList<Integer>();

        int countBefor = 0;

        //ToDo оптимизация с учетом повтора колеекций у коллекционера
        for (int i = 0; i < listNumsCollectors.size(); i++) {

            ArrayList<Integer> listD = new ArrayList<Integer>(listDiegoNomers);
            int nowlistNumsCollectors = listNumsCollectors.get(i);
            listD.add(nowlistNumsCollectors);//закинем первый номер карточек Коллекционера в список
            start1=System.currentTimeMillis();
            TreeSet<Integer> setNumsDiego = new TreeSet<Integer>(listD);
            Collections.sort(listD);
            end1=System.currentTimeMillis();
            int lastNumIndex = 0;
            int setNumsDiegolast = setNumsDiego.last();
            int setNumsDiegofirst = setNumsDiego.first();
            if (setNumsDiegolast == nowlistNumsCollectors) {
                countBefor = setNumsDiego.size()-1;
            } else if (setNumsDiegofirst == nowlistNumsCollectors) {
                countBefor = 0;

            } else {
                if (setNumsDiego.size() > 1) {
                    if (resultForCollectors.containsKey(nowlistNumsCollectors)) {
                        lastNumIndex = resultForCollectors.get(nowlistNumsCollectors);
                    } else {
                       timeStartBin = System.currentTimeMillis();
                        lastNumIndex = binsearch(setNumsDiego, nowlistNumsCollectors);
                        timeStopBin = System.currentTimeMillis();
                        speedBin += (timeStopBin-timeStartBin);
                        resultForCollectors.put(nowlistNumsCollectors, lastNumIndex);
                    }
                    countBefor = lastNumIndex > 0 ? (lastNumIndex - 1) + 1 : 0;

                } else {
                    countBefor = findStickerLess(listD.get(0), nowlistNumsCollectors);
                }
            }
            ans.add(countBefor);
            countBefor = 0;
            setNumsDiego = null;
            listD = null;

            long resultSpeed  = end1-start1;
            sumSpeed = sumSpeed+resultSpeed;
        }
        return ans;
    }

    public int binsearch(TreeSet<Integer> sortedList, int firstSticker) {
        ArrayList<Integer> list = new ArrayList<Integer>(sortedList);
        int L = 0;
        int R = list.size() - 1;
        int m = 0;
        if (firstSticker <= 0) {
            m = -1;
        }

        while (L <= R) {
            m = L + (R - L) / 2;
            int nowNum = list.get(m);
            if (firstSticker < list.get(m)) {
                R = m - 1;
            } else if (firstSticker > list.get(m)) {
                L = m + 1;
            } else {
                return m;
            }
        }
        return m;
    }

    public int findStickerLess(int n, int m) {
        int ans = 0;
        if (n < m) {
            ans = 1;
        }
        return ans;
    }

    public static void main(String[] args) throws IOException {
        long timeStart = System.currentTimeMillis();
        long timeEndInput=0;
        long timeEndAlgo = 0;
        FinderSticker finder = new FinderSticker();
        //  BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Locale.setDefault(Locale.US);
        String fileName = "./Testinput.txt";
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        int amtDiegoSticker = parseInt(reader.readLine());
        String strOfDiegoNomers;
        StringBuilder sb1 = new StringBuilder("");
        if ((strOfDiegoNomers = reader.readLine()) != null) {
            sb1.append(strOfDiegoNomers);
        } else {
            OutputStream outputStream = new FileOutputStream("output.txt");
            PrintWriter printWriter = new PrintWriter(outputStream);
            printWriter.close();
            outputStream.close();
        }
        int amtOfCollector = parseInt(reader.readLine());
        String strOfCollectorsNomers;
        StringBuilder sb2 = new StringBuilder("");
        if ((strOfCollectorsNomers = reader.readLine()) != null) {
            sb2.append(strOfCollectorsNomers);
        } else {
            OutputStream outputStream = new FileOutputStream("output.txt");
            PrintWriter printWriter = new PrintWriter(outputStream);
            printWriter.close();
            outputStream.close();
        }
        reader.close();

        ArrayList<Integer> ans = null;
        if (amtDiegoSticker == 0) {
            String[] countCollectors = strOfCollectorsNomers.trim().split(" ");
            for (int i = 0; i < countCollectors.length; i++) {
                System.out.println(0);
            }
        } else if (amtOfCollector == 0) {
            String[] countDiegoCard = strOfDiegoNomers.trim().split(" ");
            System.out.println(countDiegoCard.length);
        } else {
            timeEndInput=System.currentTimeMillis();
            ans = finder.findSticker(amtDiegoSticker, strOfDiegoNomers, amtOfCollector, strOfCollectorsNomers);
            timeEndAlgo = System.currentTimeMillis();
            Object[] arrAns = ans.toArray();

            OutputStream outputStream = new FileOutputStream("output.txt");
            PrintWriter printWriter = new PrintWriter(outputStream);
            for (Integer item : ans
            ) {
                printWriter.println(item);
            }
            printWriter.close();
            outputStream.close();
        }
        long timeEnd = System.currentTimeMillis();
        System.err.println("Time(ms) = ALL " + (timeEnd - timeStart));
        System.err.println("Time(ms) = Input " + (timeEndInput - timeStart));
        System.err.println("Time(ms) = ALGO " + (timeEndAlgo - timeStart));
        System.out.println("_________________________________");
        System.err.println("Time init = " + finder.sumSpeed);
        System.err.println("Time bin = " + finder.speedBin);

    }

}
