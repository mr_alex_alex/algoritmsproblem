package ru.alexch;

import java.beans.PropertyEditorManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.*;

public class GistogrammOfCharsInString {
    void printChars(String str) {
        int maxsymcount = 0;
        int nowcount = 0;
        Map<Character, Integer> map = new HashMap<>();
        char[] chars = str.toCharArray();
        for (int i = 0; i <= chars.length - 1; i++) {
//            if ((int)chars[i] == (' ')){
//            continue;
//            }
//            if ((int)chars[i] == ('\n')){
//                continue;
//            }
            if (!map.containsKey(chars[i])  ) {
                map.put(chars[i], 0);
            }
            nowcount = map.get(chars[i]);
            nowcount += 1;
            map.put(chars[i], nowcount);
            maxsymcount = Math.max(maxsymcount, nowcount);
        }
//        if (map.containsKey(' ')){ map.remove(' ');}
//        if (map.containsKey('\n')){ map.remove('\n');}

        Map<Character, Integer> sortedmap = new TreeMap<>(map);
        for (int i = maxsymcount; i > 0; i--) {
            for (Map.Entry<Character, Integer> entry : sortedmap.entrySet()
            ) {
                if (entry.getValue() >= i ) {
                    System.out.print('#');
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }

        Iterator<Map.Entry<Character, Integer>> itr = sortedmap.entrySet().iterator();

        StringBuilder sb = new StringBuilder();
        while (itr.hasNext()) {
            String nowstr = itr.next().getKey().toString();
            nowstr.trim();
            sb.append(nowstr);
        }
        System.out.println(sb);
    }

    public static void main(String[] args) throws IOException {
        GistogrammOfCharsInString gistogramm = new GistogrammOfCharsInString();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String str;
        StringBuilder sb = new StringBuilder("");
            while ((str = reader.readLine()) != null) {
                    sb.append(str).append(" ");
                    if (!reader.ready()){
                        break;
                    }
            }
        gistogramm.printChars(sb.toString());
            reader.close();

    }
}
