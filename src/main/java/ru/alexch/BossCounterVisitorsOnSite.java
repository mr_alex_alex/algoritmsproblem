package ru.alexch;

import java.util.ArrayList;
import java.util.Collections;

public class BossCounterVisitorsOnSite {
    public ArrayList countVisitorsOnlineByBoss(int n, int[] tin, int[] tout, int m, int[] tboss) {
        int online = 0;
        ArrayList<LabelEvent> events = new ArrayList<>();

        //TODO занести метки в ENUM
        for (int i = 0; i < n; i++) {
            events.add(new LabelEvent(tin[i], "IN"));
            events.add(new LabelEvent(tout[i], "OUT"));
        }
        for (int j = 0; j < m; j++) {
            events.add(new LabelEvent(tboss[j], "MIDL")); //имя метки по алфавиту должно быть между именем метки для входа и именем метки для выхода - для случев когда момент врмени совпадает
        }
        Collections.sort(events);
        ArrayList bossAns = new ArrayList();
        for (int i = 0; i < events.size(); i++) {

            if (events.get(i).getLabel().equals("IN")) {
                online += 1;
            } else if (events.get(i).getLabel().equals("OUT")) {
                online -= 1;
            } else {
                bossAns.add(online);
            }

        }
        return bossAns;
    }

    public static void main(String[] args) {
        BossCounterVisitorsOnSite counter = new BossCounterVisitorsOnSite();
        ArrayList ans = counter.countVisitorsOnlineByBoss(4, new int[]{10, 20, 30, 35}, new int[]{15, 30, 40, 45,}, 4, new int[]{13, 30, 39, 42});
        System.out.println(ans);
    }

    public class LabelEvent implements Comparable<LabelEvent> {
        private Long time;
        private String label;

        public Long getTime() {
            return time;
        }

        public String getLabel() {
            return label;
        }

        public LabelEvent(long time, String label) {
            this.time = time;
            this.label = label;
        }

        @Override // сначала сравниваем время, при равенстве порядок алфавитный!!!
        public int compareTo(LabelEvent o) {
            int result = this.time.compareTo(o.time);
            if (result == 0) {
                result = this.label.compareTo(o.label);
            }
            return result;
        }
    }
}
