package ru.alexch;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class RLEncoder {
    public String encode(String s) {
        int num = 0;
        char[] chars = s.toCharArray();
        char lastsymbol = chars[0];
        Map<Character, Integer> letters = new LinkedHashMap<>();
        for (int i = 0; i <= chars.length - 1; i++) {
            if (chars[i] != lastsymbol) {
                letters.put(lastsymbol, num);
                lastsymbol = chars[i];
                num = 1;
            } else {
                num++;
            }
        }
        letters.put(lastsymbol,num);
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<Character, Integer> lettter : letters.entrySet()
        ) {
            sb.append(lettter.getKey());
            if (lettter.getValue() > 1) {
                sb.append(lettter.getValue());
            }
        }
        return String.valueOf(sb);
    }
}
