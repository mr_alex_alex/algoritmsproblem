package ru.alexch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SolutionLonearSearchFromLeft {
    public static void main(String[] args) throws IOException {
        LinearSearcherFromLeft searcher = new LinearSearcherFromLeft();
        int[] array = new int[5];
        Integer findElem = 2;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        for (int i = 0; i <= array.length - 1; i++) {
            String string = reader.readLine();
            array[i]= Integer.parseInt(string);
            if (i == array.length - 1) {
                System.out.printf("Порядковый номер символа %s  равен: %s",findElem, searcher.leftSeach(array, findElem));
            }
        }
    }
}