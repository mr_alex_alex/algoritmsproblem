package ru.alexch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class SolutionFinderTwoMaxNumber {
    public static void main(String[] args) throws IOException {
        FinderTwoMaxNumber finder  = new FinderTwoMaxNumber();
        int[] sequence = new int[6];
        Integer findElem = 3;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        for (int i = 0; i <= sequence.length - 1; i++) {
            String string = reader.readLine();
            try{sequence[i] =  Integer.parseInt(string);
            }
            catch (NumberFormatException e){
                System.out.println( "Возможен ввод только чисел");
                --i;
            }
            if (i == sequence.length - 1) {
                List<Integer> result= finder.findTwoMaxNumber(sequence);
                System.out.printf("Пара наибольших чисел : %s и  %s",result.get(0),result.get(1) );
            }
        }
    }
}