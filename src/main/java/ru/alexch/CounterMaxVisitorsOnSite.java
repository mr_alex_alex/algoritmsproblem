package ru.alexch;

import java.util.ArrayList;
import java.util.Collections;

public class CounterMaxVisitorsOnSite {
    public int countMaxVisitorsOnline(int n, int[] tin, int[] tout) {
        int online = 0;
        int maxOnline = 0;
        ArrayList<LabelEvent> events = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            events.add(new LabelEvent(tin[i], "IN"));
            events.add(new LabelEvent(tout[i], "OUT"));
        }
        Collections.sort(events);
        for (LabelEvent event:events
             ) {
            if (event.getLabel().equals("IN")){
                online +=1;
            } else if (event.getLabel().equals("OUT")) {
                online -=1;
            }
        maxOnline =Math.max(maxOnline,online);
        }

        return maxOnline;
    }

    public static void main(String[] args) {
        CounterMaxVisitorsOnSite counter = new CounterMaxVisitorsOnSite();
       int ans = counter.countMaxVisitorsOnline(4,new int[] {1,2,3,4}, new int[] {4,4,4,4,});
        System.out.println(ans);
    }
    public class LabelEvent implements Comparable<LabelEvent> {
        private Long time;
        private String label;

        public Long getTime() {
            return time;
        }

        public String getLabel() {
            return label;
        }

        public LabelEvent(long time, String label) {
            this.time = time;
            this.label = label;
        }

        @Override
        public int compareTo(LabelEvent o) {
            int result = this.time.compareTo(o.time);
            if (result == 0) {
             result =this.label.compareTo(o.label);
            }
            return result;
        }
    }
}
