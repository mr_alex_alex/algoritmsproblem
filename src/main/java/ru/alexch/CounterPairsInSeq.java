package ru.alexch;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class CounterPairsInSeq {

    public static Map<Integer, Integer> countPrefixSum(int[] nums) {

//        Integer[] nums = Stream.of(seq.trim().split(" "))
//                .map(x -> Integer.parseInt(x))
//                .toArray(Integer[]::new);

        Map<Integer, Integer> prefixSumByValue = new HashMap<>();
        prefixSumByValue.put(0, 1);  // возможно что в последовательности нет суммы пар ==0
        int nowsum = 0;
        for (int i = 0; i < nums.length; i++) {
            nowsum += nums[i];
            if (!prefixSumByValue.containsKey(nowsum)) {
                prefixSumByValue.put(nowsum, 0);
            }
            int nowvalue = prefixSumByValue.get(nowsum);
            nowvalue += 1;
            prefixSumByValue.put(nowsum, nowvalue);
        }
        return prefixSumByValue;
    }

    public static int countZerosSumRangers(Map<Integer, Integer> diapasons) {
        int cntranges = 0;
        for (Map.Entry<Integer, Integer> diapason : diapasons.entrySet()
        ) {
            cntranges = diapason.getValue();
            cntranges += cntranges * (cntranges - 1) / 2;
        }
        return cntranges;
    }

    public static void main(String[] args) {
        int[] seq = new int[]{-2, 5, -3, 2, -5, 3};
      //  String seq = new String("-2 2");
        int resut = countZerosSumRangers(countPrefixSum(seq));
        System.out.println(resut);
    }
}
