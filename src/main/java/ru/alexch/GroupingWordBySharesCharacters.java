package ru.alexch;

import java.util.*;

public class GroupingWordBySharesCharacters {
    public Map<Integer, List<String>> groupWords(List<String> words) {
        Map<String, List<String>> groups = new HashMap<>();
        String nowword;
        for (String word : words
        ) {
            nowword = word;
            char[] keyword = nowword.toCharArray();
            Arrays.sort(keyword);
            String sortedword = String.valueOf(keyword);
            if (!groups.containsKey(sortedword)) {
                groups.put((sortedword), new ArrayList<>());
            }
            groups.get(sortedword).add(nowword);
        }
        Map<Integer, List<String>> ans = new TreeMap<>(Collections.reverseOrder());
        for (Map.Entry<String, List<String>> entry : groups.entrySet()
        ) {
            List sortedlist = new ArrayList<>(entry.getValue());
            Collections.sort(sortedlist);
            ans.put(sortedlist.size(), sortedlist);
        }

        return ans;
    }

}
