package ru.alexch;

import java.security.SecureRandom;

public class FinderTheShortestWords {
    public String findTheShortestWords(String[] array) {
        int minSizeWord = array[0].length();
        String ans = "";
        StringBuilder sb = new StringBuilder(ans);
        for (int i = 1; i <= array.length - 1; i++) {
            int sizeWord = array[i].length();
            if (sizeWord < minSizeWord) {
                minSizeWord = sizeWord;
            }
        }
        for (int i = 0; i <= array.length - 1; i++) {
            int sizeWord = array[i].length();
            if (sizeWord == minSizeWord) {
                sb.append(array[i]).append(" ");
            }
        }
        return String.valueOf(sb).trim();
    }
}
