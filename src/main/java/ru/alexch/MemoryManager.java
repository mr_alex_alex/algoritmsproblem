package ru.alexch;

import java.util.ArrayList;

public class MemoryManager {

    public ArrayList<MemStruct> initMemory(int maxn) {
        ArrayList<MemStruct> memory = new ArrayList<>();
        ArrayList<Node> nodes = new ArrayList<>();
        for (int i = 0; i < maxn; i++) {
            nodes.add(new Node(0, i + 1, 0));
        }
        Integer firstFree = 0;
        memory.add(new MemStruct(nodes, firstFree));
        return memory;
    }

    public Integer newNode(MemStruct memStruct) {
        ArrayList<Node> memory = memStruct.getMemory();
        Integer firstFree = memStruct.getFirstFree();
        memStruct.setFirstFree(memory.get(firstFree).getLeftSon());
        return firstFree;
    }

    public void delNode(MemStruct memStruct, Integer index) {
        ArrayList<Node> memory = memStruct.getMemory();
        Integer firstFree = memStruct.getFirstFree();
        memory.get(index).setLeftSon(firstFree);
        memStruct.setFirstFree(index);

    }

    public class MemStruct {
        private ArrayList<Node> memory;
        private Integer firstFree;

        public MemStruct(ArrayList<Node> memory, Integer firstFree) {
            this.memory = memory;
            this.firstFree = firstFree;
        }

        public ArrayList<Node> getMemory() {
            return memory;
        }

        public Integer getFirstFree() {
            return firstFree;
        }

        public void setFirstFree(Integer firstFree) {
            this.firstFree = firstFree;
        }
    }

    public class Node {
        private Integer key;
        private Integer leftSon;
        private Integer rightSon;

        public Node(Integer key, Integer leftSon, Integer rightSon) {
            this.key = key;
            this.leftSon = leftSon;
            this.rightSon = rightSon;
        }

        public Integer getLeftSon() {
            return leftSon;
        }

        public void setLeftSon(Integer leftSon) {
            this.leftSon = leftSon;
        }
    }
}
