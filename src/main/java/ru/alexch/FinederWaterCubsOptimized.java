package ru.alexch;

import com.sun.source.tree.BreakTree;

public class FinederWaterCubsOptimized {

    public int find(int[] h) {
        int maxpos = 0;
        for (int i = 0; i <= h.length - 1; i++) {
            if (h[i] > h[maxpos]) {
                maxpos = i;
            }
        }
        int ans = 0;
        int nowm = 0;
        for (int i = 0; i <= maxpos; i++) {
            if (h[i] > nowm) {
                nowm = h[i];
            }
            ans += nowm - h[i];
        }
        nowm=0;
        for (int i = h.length-1; i >=maxpos ; i--) {
            if (h[i] > nowm) {
                nowm = h[i];
            }
            ans += nowm - h[i];
        }
    return ans;
    }

}
