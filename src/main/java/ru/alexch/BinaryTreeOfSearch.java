package ru.alexch;

import javax.swing.plaf.nimbus.NimbusStyle;
import java.util.ArrayList;

public class BinaryTreeOfSearch {

    public Integer find(MemStruct memStruct, Integer root, Integer x) {
        ArrayList<Node> nodes = memStruct.getMemory();
        Integer key = nodes.get(root).getKey();
        if (x == key) {
            return root;
        } else if (x < key) {
            Integer left = nodes.get(root).getLeftSon();
            if (left == -1) {
                return -1;
            } else {
                return find(memStruct, root, x);
            }
        } else {            //(x > key)
            Integer right = nodes.get(root).getRightSon();
            if (right == -1) {
                return -1;
            } else {
                return find(memStruct, root, x);
            }
        }
    }

    public Integer createAndFillNode(MemStruct memStruct, Integer key) {
        Integer index = newNode(memStruct);
        ArrayList<Node> nodes = memStruct.getMemory();
        Node nowNode = nodes.get(index);
        nowNode.setKey(key);
        nowNode.setRightSon(-1);
        nowNode.setLeftSon(-1);
        return index;
    }

    public void add(MemStruct memStruct, Integer root, Integer x) {
        ArrayList<Node> nodes = memStruct.getMemory();
        Node nowNode = nodes.get(root);
        Integer key = nowNode.getKey();
        if (x < key) {
            Integer left = nowNode.getLeftSon();
            if (left == -1) {
                nowNode.setLeftSon(createAndFillNode(memStruct, x));
            } else {
                add(memStruct, left, x);
            }
        } else if (x > key) {
            Integer right = nowNode.getRightSon();
            if (right == -1) {
                nowNode.setRightSon(createAndFillNode(memStruct, x));
            } else {
                add(memStruct, right, x);
            }
        }
    }

    public static void main(String[] args) {
        BinaryTreeOfSearch bts = new BinaryTreeOfSearch();
        ArrayList<MemStruct> memoryStructure = bts.initMemory(20);
        Integer root = bts.createAndFillNode(memoryStructure.get(0), 8);
        bts.add(memoryStructure.get(0), root, 10);
        bts.add(memoryStructure.get(0), root, 9);
        bts.add(memoryStructure.get(0), root, 14);
        bts.add(memoryStructure.get(0), root, 13);
        bts.add(memoryStructure.get(0), root, 3);
        bts.add(memoryStructure.get(0), root, 1);
        bts.add(memoryStructure.get(0), root, 6);
        bts.add(memoryStructure.get(0), root, 4);
        bts.add(memoryStructure.get(0), root, 7);

        ArrayList<Node> nodes = memoryStructure.get(0).getMemory();
        StringBuilder sb = new StringBuilder();
        for (Node node : nodes
        ) {
            String leftS;
            String rightS;
            if (node.getKey() != 0) {
                //в поле leftSon RightSon - хранится индекс элемента в структуое памяти
                if (node.getLeftSon() == -1) {
                    leftS = "-1";
                } else {
                    leftS = String.valueOf(nodes.get(node.getLeftSon()).getKey());
                }
                if (node.getRightSon() == -1) {
                    rightS = "-1";
                } else {
                    rightS = String.valueOf(nodes.get(node.getRightSon()).getKey());
                }
                sb.append("***" + leftS + "___" + node.getKey() + "___" + rightS + "***");
                rightS = null;
                leftS = null;
            }
        }
        System.out.println("Дерево построено:  " + sb);
    }

    public ArrayList<MemStruct> initMemory(int maxn) {
        ArrayList<MemStruct> memory = new ArrayList<>();
        ArrayList<Node> nodes = new ArrayList<>();
        for (int i = 0; i < maxn; i++) {
            nodes.add(new Node(0, i + 1, 0));
        }

        ArrayList list = new ArrayList();
        Integer firstFree = 0;
        memory.add(new MemStruct(nodes, firstFree));
        return memory;
    }

    public Integer newNode(MemStruct memStruct) {
        ArrayList<Node> memory = memStruct.getMemory();
        Integer firstFree = memStruct.getFirstFree();
        memStruct.setFirstFree(memory.get(firstFree).getLeftSon());
        return firstFree;
    }

    public void delNode(MemStruct memStruct, Integer index) {
        ArrayList<Node> memory = memStruct.getMemory();
        Integer firstFree = memStruct.getFirstFree();
        memory.get(index).setLeftSon(firstFree);
        memStruct.setFirstFree(index);

    }

    public class MemStruct {
        private ArrayList<Node> memory;
        private Integer firstFree;

        public MemStruct(ArrayList<Node> memory, Integer firstFree) {
            this.memory = memory;
            this.firstFree = firstFree;
        }

        public ArrayList<Node> getMemory() {
            return memory;
        }

        public Integer getFirstFree() {
            return firstFree;
        }

        public void setFirstFree(Integer firstFree) {
            this.firstFree = firstFree;
        }



    }

    public class Node {
        private Integer key;
        private Integer leftSon;
        private Integer rightSon;

        public Node(Integer key, Integer leftSon, Integer rightSon) {
            this.key = key;
            this.leftSon = leftSon;
            this.rightSon = rightSon;
        }

        public Integer getLeftSon() {
            return leftSon;
        }

        public Integer getKey() {
            return key;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public Integer getRightSon() {
            return rightSon;
        }

        public void setRightSon(Integer rightSon) {
            this.rightSon = rightSon;
        }

        public void setLeftSon(Integer leftSon) {
            this.leftSon = leftSon;
        }

    }

}
