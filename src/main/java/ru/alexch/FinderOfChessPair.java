package ru.alexch;

import javax.swing.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FinderOfChessPair {

    Map<Integer, Integer> mapRow = new HashMap<>();
    Map<Integer, Integer> mapColumn = new HashMap<>();

    public int findCountPairFightRook(Set<Coordinat> coordinats) {
        for (Coordinat coordinat : coordinats
        ) {
            //       номер строки и индекс ладьи в строке  ;  номер столбцв и индекс ладьи в столбце
            addRook(mapRow, coordinat.getI());
            addRook(mapColumn, coordinat.getJ());
        }

        return countpairs(mapRow) + countpairs(mapColumn);
    }
    // инрементируем значение на 1  в словаре с ключом = равным номеру переданной строки или столбца
    private void addRook(Map<Integer, Integer> map, Integer key) {
        int nowcount = 0;
        if (!map.containsKey(key)) {
            map.put(key, 0);
        }
        nowcount = map.get(key);
        nowcount += 1;
        map.put(key, nowcount);
    }
 // количестов пар равно количеству фигур в словаре - 1
    private int countpairs(Map<Integer, Integer> map) {
        int pairs = 0;
        for (Map.Entry<Integer, Integer> row : map.entrySet()
        ) {
            pairs += row.getValue() - 1;
        }

        return pairs;
    }
}
