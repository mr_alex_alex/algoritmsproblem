package ru.alexch;

import java.util.Arrays;

public class Fibonacci {


    public static void main(String[] args) {

        int s = 45;
        long[] accum = new long[s+1];
        Arrays.fill(accum,-1);

           System.out.println(recurseCalculate(s, accum));
           System.out.println(calculate(s));
    }

    private static long recurseCalculate(int n, long[] accum ) {
        if (accum[n] != - 1) {
            return accum[n];
        }
        if (n<=1){
            return n;
        }
        long result =(recurseCalculate(n - 1,accum) + recurseCalculate(n - 2,accum));
        accum[n] = result;
        return result;
    }

    private static long calculate(int n) {
        long[] mem = new long[n + 1];
        mem[0] = 0;
        mem[1] = 1;
        for (int i = 2; i < n + 1; i++) {
            mem[i] = mem[i - 1] + mem[i - 2];
        }
        return mem[n];
    }
}
