package ru.alexch;

public class FinderNumberWaterCube {
    char isRockCube = 'n';
    private int indexOfMaxHightOfPillar;

    public int findNumberWaterCube(String[] seq) {
        indexOfMaxHightOfPillar = 0;
        int maxHightOfPillar = 0; //seq[0].length();
        int[] pillar = new int[seq.length];
        for (int i = 0; i <= seq.length - 1; i++) {
            pillar[i] = countRockCubeInPillar(seq[i]);
            if (pillar[i] > maxHightOfPillar) {
                maxHightOfPillar = pillar[i];
                indexOfMaxHightOfPillar = i;
            }
        }
        int countOfWaterCubes = findWaterCubeFromSeqLeft(pillar, indexOfMaxHightOfPillar, 0)
                + findWaterCubeFromSeqRight(pillar, indexOfMaxHightOfPillar, pillar.length - 1);
        return countOfWaterCubes;
    }

    public int countRockCubeInPillar(String pillar) {
        int rockCubePillar = 0;
        StringBuilder sb = new StringBuilder(pillar);
        char[] cubes = new char[sb.length()];
        sb.getChars(0, sb.length(), cubes, 0);
        if (Character.compare(cubes[0], isRockCube) == 0) {
            for (int j = 0; j <= cubes.length - 1; j++) {
                rockCubePillar++;
            }
        }
        return rockCubePillar;
    }

    public int findWaterCubeFromSeqRight(int[] pillar, int indexMaxFirst, int indexMaxSecond) {
        int CountofWaterCubes = 0;
        int maxHightOfPillarFromRight = 0;
        int indexHightOfPillarFromRight = indexMaxFirst;
        for (int i = indexMaxFirst + 1; i <= indexMaxSecond; i++) {
            if (pillar[i] >= maxHightOfPillarFromRight) {
                maxHightOfPillarFromRight = pillar[i];
                indexHightOfPillarFromRight = i;
            }
        }
        int CountofWaterCubesStep = Math.min(pillar[indexMaxFirst], maxHightOfPillarFromRight)
                * (indexHightOfPillarFromRight - indexMaxFirst)
                - CountOfRockCubesInSeq(pillar, indexMaxFirst + 1, indexHightOfPillarFromRight);
        if (indexHightOfPillarFromRight == (pillar.length - 1)) {
            return CountofWaterCubes + CountofWaterCubesStep;
        }
        CountofWaterCubes = CountofWaterCubesStep + findWaterCubeFromSeqRight(pillar, indexHightOfPillarFromRight, pillar.length - 1);
        return CountofWaterCubes;
    }

    public int findWaterCubeFromSeqLeft(int[] pillar, int indexMaxFirst, int indexMaxSecond) {
        int CountofWaterCubes = 0;
        int maxHightOfPillarFromLeft = 0;
        int indexHightOfPillarFromLeft = indexMaxFirst;
        for (int i = indexMaxFirst - 1; i >= indexMaxSecond; i--) {
            if (pillar[i] >= maxHightOfPillarFromLeft) {
                maxHightOfPillarFromLeft = pillar[i];
                indexHightOfPillarFromLeft = i;
            }
        }
        int CountofWaterCubesStep = Math.min(pillar[indexMaxFirst], maxHightOfPillarFromLeft)
                * (indexMaxFirst - indexHightOfPillarFromLeft)
                - CountOfRockCubesInSeq(pillar, indexHightOfPillarFromLeft, indexMaxFirst - 1);
        if (indexHightOfPillarFromLeft == 0) {
            return CountofWaterCubes + CountofWaterCubesStep;
        }
        CountofWaterCubes = CountofWaterCubesStep + findWaterCubeFromSeqLeft(pillar,
                indexHightOfPillarFromLeft, 0);

        return CountofWaterCubes;
    }

    public int CountOfRockCubesInSeq(int[] pillar, int startIndex, int endIndex) {
        int count = 0;
        for (int i = startIndex; i <= endIndex; i++) {
            count = count + pillar[i];
        }
        return count;
    }
}


