package ru.alexch;

import java.io.IOException;

public class LinearSearcherFromLeft {
    Integer ans = -1;

    public  int leftSeach(int[] array, Integer elem) throws IOException {

        for (int i = 0; i <= array.length-1; i++) {
            if ((elem.equals(array[i])) && (ans == -1)) {
                ans = i;
            }
        }
        return ans;
    }
}
