package ru.alexch.homework.gameminers;

import java.util.*;

public class Field {

    private String[][] kletkas;
    public void makeGameFields(int n, int m, List<Mine> mines) {
        //массивы сдвига - клетки вокруг мины
        int[] di = new int[]{-1, -1, -1, 0, 0, 1, 1, 1};
        int[] dj = new int[]{-1, 0, 1, -1, 1, -1, 0, 1};
        kletkas = new String[n + 2][m + 2];

        //с учетом рамки по перметру в 1 клетку шириной
        for (int i = 0; i < n + 2; i++) {
            for (int j = 0; j < m + 2; j++) {
                kletkas[i][j] = "0";
            }
        }
        for (int l = 0; l < mines.size(); l++) {
            kletkas[mines.get(l).getI()+1][mines.get(l).getJ()+1] ="*";
            for (int k = 0; k < di.length; k++) {
                //увеличиваем счетчик в  клетке, окружающей мину
                // с индексом равным индексу мины + индексы сдвига
                int nowKletkaIndexI = mines.get(l).getI() + di[k] + 1;
                int nowKletkaIndexJ = mines.get(l).getJ() + dj[k] + 1;
                int now = Integer.parseInt(kletkas[nowKletkaIndexI][nowKletkaIndexJ]);
                now +=1;
                kletkas[nowKletkaIndexI][nowKletkaIndexJ] = String.valueOf(now);
            }
        }
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                System.out.print(kletkas[i][j] + " ");
            }
            System.out.println();
        }
    }
}
