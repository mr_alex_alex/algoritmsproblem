package ru.alexch.homework.gameminers;

public class Mine {
    private int i;
    private int j;
    private static String pict = "*";

    public Mine(int i, int j) {
        this.i = i;
        this.j = j;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public int getJ() {
        return j;
    }

    public void setJ(int j) {
        this.j = j;
    }
}
