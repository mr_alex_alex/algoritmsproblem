package ru.alexch.homework;

public class TrianglerMaximusa {

   public String findNote(int n, float prev, String str) {
       float left = 30f;
       float right = 4000f;
       String[] notes =  str.trim().split(" ");

       for (int i = 0; i < n-1 ; i=i+2) {
           float now = Float.valueOf(notes[i]);
           //______________________  костыль - может косяк с вещественным числом  - не проходились тесты
           if (Math.abs(now-prev) < Math.pow(10,-6)){
               continue;
           }
           // _____________________
           if (notes[i+1].equals("closer")){
               if (now > prev){
                   left = Math.max(left , (now+prev)/2) ;
               }else {
                   right= Math.min(right ,(now+prev)/2 );
               }
           }else{
               if (now < prev){
                   left = Math.max(left , (now+prev)/2) ;
               }else {
                   right= Math.min(right ,(now+prev)/2 );
               }
           }
           prev = now;
       }
       return left +" "+ right;
   }

    public static void main(String[] args) {
        TrianglerMaximusa solution = new TrianglerMaximusa();

        System.out.println(solution.findNote(3, 440.0f,"220.00 closer 300.00 further"));



    }
}
