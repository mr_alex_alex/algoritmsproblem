package ru.alexch.homework;

import java.util.Arrays;
import java.util.stream.Stream;

public class FinderSimmetricSeq {
    public int[] findSeq(int[] seq) {
        int[] ans =null;
        for (int start = 0; start < seq.length; start++) {
            int i = start;
            int j = seq.length - 1;
            while ((i < seq.length) & (j >= 0) & (seq[i] == seq[j]) & (i <= j)) {
                i += 1;
                j -= 1;
            }
            //перехлест итераторов по массиву, когда  i стал больше j, индекс j - стал лев краем симметрии
            //отражаем все что до него
            if (i > j) {
                ans = Stream.of(seq, reverseArray(seq, j))
                        .flatMapToInt(Arrays::stream)
                        .toArray();
                break;
            }
        }
    return ans;
    }
    public int[] reverseArray(int[] array, int index) {
        int[] ans = new int[index];

        for (int k = 0; k <= index-1  ; k++ ){
            ans[k]= array[index-k-1];
        }
        return ans;
    }

}
