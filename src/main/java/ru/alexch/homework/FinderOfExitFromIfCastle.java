package ru.alexch.homework;

//Узник замка Иф
public class FinderOfExitFromIfCastle {
    public boolean findIfTrue(int[] sizeHole, int[] sizeOfBrick) {
        boolean ans;
        int sizeHoleA = sizeHole[0];
        int sizeHoleB = sizeHole[1];
        int sizeOfBrickA = sort(sizeOfBrick)[0];
        int sizeOfBrickB = sort(sizeOfBrick)[1];
        ans = (Math.max(sizeHoleA, sizeHoleB) > Math.max(sizeOfBrickA, sizeOfBrickB) &
                Math.min(sizeHoleA, sizeHoleB) > Math.min(sizeOfBrickA, sizeOfBrickB));
        return ans;
    }

    public int[] sort(int[] array) {
        for (int i = 0; i < array.length-1; i++) {
            if (array[0] > array[i + 1]) {
                int tmp = array[i + 1];
                array[i + 1] = array[0];
                array[0] = tmp;
            }
        }
        return array;
    }

}
