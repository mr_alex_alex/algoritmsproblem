package ru.alexch.homework;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FinderMaxOfMultiplicationThreeNumber {
    private static void kth_rearrange(int[] seq, int left, int right, int k) {
        //eqxfirst - первое число равное x
        //gtxfirst - первое число больше x
        left = 0;
        right = seq.length - 1;
        while (left < right) {
            int x = seq[(right + left) / 2];
            int eqxfirst = left;
            int gtxfirst = left;
            for (int i = left; i < right + 1; i++) {
                int now = seq[i];
                if (now == x) {
                    // свопаем и отодвигаем границу вправо
                    seq[i] = seq[gtxfirst];
                    seq[gtxfirst] = now;
                    gtxfirst += 1;
                    //в итоге диапазон границ элементов равных x  сдвигаем вправо на  1
                } else if (now < x) {
                    seq[i] = seq[gtxfirst];
                    seq[gtxfirst] = seq[eqxfirst];
                    seq[eqxfirst] = now;
                    gtxfirst += 1;
                    eqxfirst += 1;
                }
            }
            //если берем следующий k левее(или правее) диапазона равных элементов X предыдущего K
            if (k < eqxfirst) {
                right = eqxfirst - 1;
            } else if (k >= gtxfirst) {
                left = gtxfirst;
            }else {
                return;
            }
        }
    }

    public static void main(String[] args) {
        int[] seq = new int[]{23,1,10,9,7,12,0,6,3,3,3,3};

        // наибольшее произвведение могут дать как произведение трех максимальных чисел  (3 крайних справа в отсортированной последовательности)
        // так и произведение  одного максимального и два минимальных отрицательных  (1 правый  и 2 крайних левых в отсортированной последовательности)
        kth_rearrange(seq,0, seq.length, seq.length-1);
        System.out.println(Arrays.toString(seq));
        kth_rearrange(seq,0, seq.length-1, seq.length-2);
        System.out.println(Arrays.toString(seq));
        kth_rearrange(seq,0, seq.length-3, 2);
        System.out.println(Arrays.toString(seq));

        int var1 =  seq[seq.length-1] *seq[seq.length-2]*seq[seq.length-3];
        int var2 =   seq[seq.length-1] *seq[0]*seq[1];

        if (var1>var2 ){
            System.out.println(seq[seq.length-1] *seq[seq.length-2]*seq[seq.length-3]);
        }else{
            System.out.println(seq[seq.length-1] *seq[0]*seq[1]);
        }

    }

}
