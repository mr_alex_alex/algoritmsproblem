package ru.alexch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FinderTwoMaxNumber {

    public List<Integer> findTwoMaxNumber(int[] seq) {

        List<Integer> results = new ArrayList<>();
        int ans1 = Math.max(seq[0], seq[1]);
        int ans2 = Math.min(seq[0], seq[1]);
        for (int i = 2; i <= seq.length - 1; i++) {
            if (seq[i] > ans1) {
                ans2 = ans1;
                ans1 = seq[i];
            } else if ((seq[i] > ans2) || (seq[i] == ans1)) {
                ans2 = seq[i];
            }
        }
        results.add(ans1);
        results.add(ans2);

        return results;

    }
}
