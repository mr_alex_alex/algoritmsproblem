package ru.alexch;

import java.util.ArrayList;
import java.util.Collections;

public class CounterTimeWithVisitorsOnSite {
    public int countTimeVisitorsOnline(int n, int[] tin, int[] tout) {
        int online = 0;
        int notEmptyTime = 0;
        ArrayList<LabelEvent> events = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            events.add(new LabelEvent(tin[i], "IN"));
            events.add(new LabelEvent(tout[i], "OUT"));
        }
        Collections.sort(events);

        for (int i = 0; i < events.size(); i++) {
            if (online > 0) {
                notEmptyTime += events.get(i).getTime() - events.get(i - 1).getTime();
            }

            if (events.get(i).getLabel().equals("IN")) {
                online += 1;
            } else if (events.get(i).getLabel().equals("OUT")) {
                online -= 1;
            }

        }
        return notEmptyTime;
    }

    public static void main(String[] args) {
        CounterTimeWithVisitorsOnSite counter = new CounterTimeWithVisitorsOnSite();
        int ans = counter.countTimeVisitorsOnline(4, new int[]{10, 20, 31, 35}, new int[]{15, 30, 40, 45,});
        System.out.println(ans);
    }

    public class LabelEvent implements Comparable<LabelEvent> {
        private Long time;
        private String label;

        public Long getTime() {
            return time;
        }

        public String getLabel() {
            return label;
        }

        public LabelEvent(long time, String label) {
            this.time = time;
            this.label = label;
        }

        @Override
        public int compareTo(LabelEvent o) {
            int result = this.time.compareTo(o.time);
            if (result == 0) {
                result = this.label.compareTo(o.label);
            }
            return result;
        }
    }
}
