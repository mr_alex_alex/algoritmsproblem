package ru.alexch;

public class Coordinat {
        private Integer I;
        private Integer J;

        public Coordinat(Integer i, Integer j) {
            I = i;
            J = j;
        }

        public Integer getI() {
            return I;
        }
        public void setI(Integer i) {
            I = i;
        }
        public Integer getJ() {
            return J;
        }

        public void setJ(Integer j) {
            J = j;
        }
}
