package ru.alexch;

public class FinderMinEvenNumber {
//    public int findMinEvenNumber(int[] seq) {
//        int ans = -1;
//        for (int i = 0; i <= seq.length - 1; i++) {
//            if ((seq[i] % 2 == 0) && ( (ans == -1) || (seq[i] < ans))) {
//                ans = seq[i];
//            }
//        }
//        return ans;
//    }
    //Оптимизация: вдруг надо искать нечетное число - огда используем булевский флаг
    public int findMinEvenNumber(int[] seq) {
        boolean weHaveEven = false;
        int ans =-1;
        for (int i = 0; i <= seq.length - 1; i++) {
            if ((seq[i] % 2 == 0) && ( (!weHaveEven ) || (seq[i] < ans) )){
                    ans = seq[i];
                    weHaveEven = true;
                }
            }
        return ans;
   }
}
