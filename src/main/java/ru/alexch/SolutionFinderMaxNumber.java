package ru.alexch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SolutionFinderMaxNumber {
    public static void main(String[] args) throws IOException {
         FinderMaxNumber finderMaxNumber = new FinderMaxNumber();
        int[] array = new int[6];
        Integer findElem = 3;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        for (int i = 0; i <= array.length - 1; i++) {
            String string = reader.readLine();
            try{array[i] =  Integer.parseInt(string);
            }
            catch (NumberFormatException e){
                System.out.println( "Возможен ввод только чисел");
                --i;
            }
            if (i == array.length - 1) {
                System.out.printf("Наибольшее число равно: %s", finderMaxNumber.finderMax(array));
            }
        }
    }
}