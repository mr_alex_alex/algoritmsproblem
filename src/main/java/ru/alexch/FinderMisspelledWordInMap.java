package ru.alexch;

import java.util.*;

public class FinderMisspelledWordInMap {
    public List findWord(List<String> dict, List<String> text){
        HashSet<String> goodwords = new HashSet<String>(dict);
        for (String word:dict
             ) {
            for (int delpos = 0; delpos <= word.length()-1;delpos++) {
                StringBuilder sb = new StringBuilder(word);
                sb.delete(delpos, delpos+1);
                goodwords.add(String.valueOf(sb));
            }
        }
    List ans = new ArrayList();
        for (String word:text
             ) {
            if (goodwords.contains(word)){
                ans.add(word);
            }
        }
     return ans;
    }
}
