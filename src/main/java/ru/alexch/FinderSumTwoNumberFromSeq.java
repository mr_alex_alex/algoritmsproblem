package ru.alexch;

import java.sql.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.stream.Collectors;

public class FinderSumTwoNumberFromSeq {


    public int[] findPair(int[] seq, int X){
        HashSet<Integer> set = new HashSet<Integer>();
        for (int i = 0; i <= seq.length-1; i++) {
            if ((X - seq[i]) != (seq[i])) {
                if (set.contains(X - seq[i])) {
                    return new int[]{(X - seq[i]), seq[i]};
                }
                set.add(seq[i]);
            }
        }
        return new int[]{0,0};
        }


}
