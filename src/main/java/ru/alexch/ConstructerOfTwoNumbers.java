package ru.alexch;

public class ConstructerOfTwoNumbers {
    public boolean IfEqualsTwoSequenceOfDigit(int seq1, int seq2) {
        int[] digitseq1 = toConstructSeq(seq1);
        int[] digitseq2 = toConstructSeq(seq2);
        for (int i = 0; i <= 9; i++) {
            if (digitseq1[i] != digitseq2[i]) {
                return false;
            }
        }
        return true;
    }

    public int[] toConstructSeq(int num) {
        int lastdigit = 0;
        int[] digitseq = new int[10];  // цифры  0..9
        while (num > 0) {
            lastdigit = num % 10; //получили индекс для последнего числа
            digitseq[lastdigit] += 1;       // накапливаем количество таких цифр
            num /= 10;   //отрезали последнее число
        }
        return digitseq;
    }

}
