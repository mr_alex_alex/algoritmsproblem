package ru.alexch;

public class FinderMaxNumber {

//
//    public int finderMax(int[] array) {
//        int ans = array[0];
//        for (int i = 0; i <= array.length - 1; i++) {
//            if (array[i] > ans){
//                ans = array[i];
//            }
//        }
//        return ans;
//    }

// оптимизация : начинаем цикл с 1-го элемента; запоминаем в цикле не объект а индекс
    public int finderMax(int[] array) {
        int ans = 0;
        for (int i = 1; i <= array.length-1; i++) {
            if (array[i] > array[ans]){
                ans = i;
            }
        }
        return array[ans];
    }
}
