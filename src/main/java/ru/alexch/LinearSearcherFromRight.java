package ru.alexch;

import java.io.IOException;

public class LinearSearcherFromRight {
    Integer ans = -1;

    public  int rightSeach(int[] array, Integer elem) throws IOException {

        for (int i = 0; i <= array.length-1; i++) {
            if (elem.equals(array[i])) {
                ans = i;
            }
        }
        return ans;
    }
}
