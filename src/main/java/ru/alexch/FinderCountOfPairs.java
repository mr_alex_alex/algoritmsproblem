package ru.alexch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Stream;

public class FinderCountOfPairs {

    public static int findCountPairs(String sortedseq,int k){
        int cntpairs =0;
        int last =0 ;
        Integer[] sortednums = Stream.of(sortedseq.trim().split(" "))
                .map(x -> Integer.parseInt(x))
                .toArray(Integer[]::new);
        for (int first = 0; first < sortednums.length ; first++) {
            while ((last<sortednums.length )&&(sortednums[last]-sortednums[first]<k)) {
                last +=1;
            }
            cntpairs +=sortednums.length-last;
        }
    return cntpairs;
    }

    public static void main(String[] args) throws IOException {
        FinderZeroinRange finder = new FinderZeroinRange();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String str;
        StringBuilder sb = new StringBuilder("");
         int k = Integer.parseInt(reader.readLine());
        while ((str = reader.readLine()) != null) {
            sb.append(str).append(" ");
            if (!reader.ready()){
                break;
            }
        }
        int result = findCountPairs(sb.toString(),k);
        System.out.println(result);
        reader.close();
    }
}
