package ru.alexch;

import java.util.ArrayList;
import java.util.Collections;

public class MinOfCounterLoadingOfParking {
    public int minCarsOnFullParking(ArrayList<Car> cars, int n) {
        int online = 0;
        ArrayList<Event> events = new ArrayList<>();
        //TODO занести метки в ENUM
        for (int i = 0; i < cars.size(); i++) {
            Car nowCar = cars.get(i);
            events.add(new Event(nowCar.getTimeIn(), "IN", nowCar.getPlaceTo() - nowCar.getPlaceFrom() + 1));
            events.add(new Event(nowCar.getTimOut(), "OUT", nowCar.getPlaceTo() - nowCar.getPlaceFrom() + 1));
        }
        Collections.sort(events);
        int occupied = 0;
        int nowcars = 0;
        int mincars = cars.size() + 1;
        for (int i = 0; i < events.size(); i++) {

            if (events.get(i).getLabel().equals("OUT")) {
                occupied -= events.get(i).getNumOfOccupiedPlaces();
                nowcars -= 1;
            } else if (events.get(i).getLabel().equals("IN")) {
                occupied += events.get(i).getNumOfOccupiedPlaces();
                nowcars += 1;
            }
            if (occupied == n) {
               mincars = Math.min(mincars,nowcars);
            }
        }
        return mincars;
    }

    public static void main(String[] args) {
        MinOfCounterLoadingOfParking counter = new MinOfCounterLoadingOfParking();
        ArrayList cars = counter.creatorCarsList();
        int ans = counter.minCarsOnFullParking(cars, 10);
        System.out.println(ans);
    }

    public ArrayList creatorCarsList() {
        ArrayList cars = new ArrayList();
        cars.add(new Car(1L, 8L, 1, 1));
        cars.add(new Car(0L, 5L, 2, 3));
        cars.add(new Car(0L, 3L, 4, 7));
        cars.add(new Car(4L, 5L, 4, 7));
        cars.add(new Car(3L, 7L, 8, 9));
        cars.add(new Car(2L, 4L, 10, 10));
        cars.add(new Car(4L, 6L, 10, 10));


        return cars;
    }

    public class Event implements Comparable<Event> {
        private Long time;
        private String label;
        private Integer numOfOccupiedPlaces;

        public Event(Long time, String label, Integer numOfOccupiedPlaces) {
            this.time = time;
            this.label = label;
            this.numOfOccupiedPlaces = numOfOccupiedPlaces;
        }

        public Long getTime() {
            return time;
        }

        public String getLabel() {
            return label;
        }

        public Integer getNumOfOccupiedPlaces() {
            return numOfOccupiedPlaces;
        }

        @Override // сначала сравниваем время, при равенстве времен сравниваем метки - порядок алфавитный!!!
        public int compareTo(Event o) {
            int result = this.time.compareTo(o.time);
            if (result == 0) {
                result = this.label.compareTo(o.label);
            }
            return result;
        }
    }

    public class Car {
        private Long timeIn;
        private Long timOut;
        private Integer placeFrom;
        private Integer placeTo;

        public Car(Long timeIn, Long timOut, Integer placeFrom, Integer placeTo) {
            this.timeIn = timeIn;
            this.timOut = timOut;
            this.placeFrom = placeFrom;
            this.placeTo = placeTo;
        }

        public void setTimeIn(Long timeIn) {
            this.timeIn = timeIn;
        }

        public Long getTimeIn() {
            return timeIn;
        }

        public Long getTimOut() {
            return timOut;
        }

        public Integer getPlaceFrom() {
            return placeFrom;
        }

        public Integer getPlaceTo() {
            return placeTo;
        }

        public void setTimOut(Long timOut) {
            this.timOut = timOut;
        }

        public void setPlaceFrom(Integer placeFrom) {
            this.placeFrom = placeFrom;
        }

        public void setPlaceTo(Integer placeTo) {
            this.placeTo = placeTo;
        }
    }
}
