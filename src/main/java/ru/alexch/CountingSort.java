package ru.alexch;

import java.util.Arrays;

public class CountingSort {

    public int[] countAndSort(int[] seq) {

        int minval = Arrays.stream(seq).min().getAsInt(); // сдвиг начального индекса счетчика
        int maxval = Arrays.stream(seq).max().getAsInt();
        int k = maxval - minval + 1;   // количество цифр
        int[] count = new int[k];
        int nowpos = 0;
        for (int i = 0; i < seq.length; i++) {
            count[seq[i] - minval] += 1;
        }
        for (int val = 0; val < k; val++) {
            for (int i = 0; i < count[val]; i++) {
                seq[nowpos] = val + minval;
                nowpos += 1;                         // инкремент для получения последовательного индекса при каждой записи в массив
            }
        }
        return seq;
    }
}
