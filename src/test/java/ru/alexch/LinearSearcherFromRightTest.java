package ru.alexch;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LinearSearcherFromRightTest {
    @Test
    void testOneElement() {
        LinearSearcherFromRight searcher = new LinearSearcherFromRight();
        Integer expected = 0;

        try {
            int result = searcher.rightSeach(new int[]{2}, 2);
            assertEquals(expected, result);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    @Test
    void testFirstElement() {
        LinearSearcherFromRight searcher = new LinearSearcherFromRight();
        Integer expected = 4;

        try {
            int result = searcher.rightSeach(new int[]{2,1,1,3,2}, 2);
            assertEquals(expected, result);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    @Test
    void testLastElement() {
        LinearSearcherFromRight searcher = new LinearSearcherFromRight();
        Integer expected = 4;

        try {
            int result = searcher.rightSeach(new int[]{1, 2, 1, 3, 2}, 2);
            assertEquals(expected, result);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    @Test
    void testNormal() {
        LinearSearcherFromRight searcher = new LinearSearcherFromRight();
        Integer expected = 3;

        try {
            int result2 = searcher.rightSeach(new int[]{1, 2, 1, 2, 1}, 2);
            assertTrue(expected == result2);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}