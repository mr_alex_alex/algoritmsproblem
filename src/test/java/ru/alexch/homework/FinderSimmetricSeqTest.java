package ru.alexch.homework;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class FinderSimmetricSeqTest {


    @ParameterizedTest
    @MethodSource("getValidatioArguments")
    void testFindSeq(int[] data, int[] expected) {
        FinderSimmetricSeq finder = new FinderSimmetricSeq();
        int[] actual = finder.findSeq(data);
        assertArrayEquals(expected, actual);
    }

    static Stream<Arguments> getValidatioArguments() {
        return Stream.of(
               // Arguments.of(new int[]{1, 2, 3, 4, 5, 4}, new int[]{1, 2, 3, 4, 5, 4, 3, 2, 1}),
                Arguments.of(new int[]{1, 2, 1, 0, 1, 4,8,4,2,0}, new int[]{1, 2, 1, 1, 5, 4, 3, 2, 1})
        );
    }
}