package ru.alexch.homework;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class FinderOfExitFromIfCastleTest {

    @ParameterizedTest
    @MethodSource("getVerificationArguments")
    void findIfTrue(int[] sizeHole, int[] sizeBrick, boolean expected ) {
       FinderOfExitFromIfCastle finder = new FinderOfExitFromIfCastle();
       boolean actual =  finder.findIfTrue(sizeHole,sizeBrick);
        assertEquals(expected, actual );
    }
     static Stream<Arguments> getVerificationArguments(){
        return Stream.of(Arguments.of(new int[]{5,20},new int[]{4,19,30},true ),
                Arguments.of(new int[]{20,5},new int[]{4,19,30},true ),
                Arguments.of(new int[]{20,20},new int[]{4,19,30},true ),
                Arguments.of(new int[]{5,20},new int[]{4,20,30},false ),
                Arguments.of(new int[]{5,20},new int[]{4,19,18},true ),
                Arguments.of(new int[]{5,20},new int[]{19,19,30},false ),
                Arguments.of(new int[]{20,20},new int[]{19,19,30},true ),
                Arguments.of(new int[]{20,20},new int[]{19,20,30},false ),
                Arguments.of(new int[]{20,20},new int[]{16,16,16},true )
        );
     }


}