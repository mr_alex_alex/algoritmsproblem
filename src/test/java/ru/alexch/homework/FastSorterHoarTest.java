package ru.alexch.homework;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import ru.alexch.algoritms.FastSorterHoar;
import ru.alexch.training30.FinderStickerOptimV1;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class FastSorterHoarTest {
        @ParameterizedTest
        @MethodSource("getValidationArguments")
        void testQuickSort(int[] array,int[] ans) {

            assertArrayEquals(ans,FastSorterHoar.quickSort(array));
        }

        static Stream<Arguments> getValidationArguments() {
            return Stream.of(
                    Arguments.of(new int[]{1, 2, 1, 0, 1, 4,8,4,2,0}, new int[]{0,0,1,1,1,2,2,4,4,8}),
                    Arguments.of(new int[]{100,50,1}, new int[]{1,50,100}),
                    Arguments.of(new int[]{50,1,100}, new int[]{1,50,100})

            );
        }

}