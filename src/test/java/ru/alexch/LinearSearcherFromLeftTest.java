package ru.alexch;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LinearSearcherFromLeftTest {
    @Test
    void testOneElement() {
        LinearSearcherFromLeft searcher = new LinearSearcherFromLeft();
        Integer expected = 0;

        try {
            int result = searcher.leftSeach(new int[]{2}, 2);
            assertEquals(expected, result);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    @Test
    void testFirstElement() {
        LinearSearcherFromLeft searcher = new LinearSearcherFromLeft();
        Integer expected = 0;

        try {
            int result = searcher.leftSeach(new int[]{2,1,1,3,2}, 2);
            assertEquals(expected, result);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    @Test
    void testLastElement() {
        LinearSearcherFromLeft searcher = new LinearSearcherFromLeft();
        Integer expected = 4;

        try {
            int result = searcher.leftSeach(new int[]{1, 3, 1, 3, 2}, 2);
            assertEquals(expected, result);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    @Test
    void TestNormal() {
        LinearSearcherFromLeft searcher = new LinearSearcherFromLeft();
        Integer expected2 = 1;

        try {
            int result2 = searcher.leftSeach(new int[]{1, 2, 1, 2, 1}, 2);
            assertTrue(expected2 == result2);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}