package ru.alexch;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class FinederWaterCubsOptimizedTest {

    @ParameterizedTest
    @MethodSource("getValidationArguments")
    void find(int[] h , int expected ) {
        FinederWaterCubsOptimized finder = new FinederWaterCubsOptimized();
        int actual = finder.find(h);
        assertEquals(expected,actual);
    }
    static  Stream<Arguments> getValidationArguments(){
        return Stream.of(
                Arguments.of(new int[]{2,1,3},1),
                Arguments.of(new int[]{2,1,3,1,2},2),
                Arguments.of(new int[]{2,3,1,1,4,1,1,3},8),
                Arguments.of(new int[]{1,3,1,2,1,3,1,1,2,4,1,2},11)
        );
    }
}