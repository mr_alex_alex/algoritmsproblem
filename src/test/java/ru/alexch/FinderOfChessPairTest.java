package ru.alexch;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class FinderOfChessPairTest {



    @ParameterizedTest
    @MethodSource("getArgumentsEq")
    void ifListOfRooks(Set set,int expected) {
        FinderOfChessPair finder = new FinderOfChessPair();
        int actual = finder.findCountPairFightRook(set);
        assertEquals(expected,actual);
    }



    static Stream<Arguments> getArgumentsEq(){
        Set<Coordinat> list = new HashSet<Coordinat>();
        return Stream.of(
                Arguments.of(
                        Set.of(new Coordinat(0,5),
                               new Coordinat(3,5),
                               new Coordinat(5,5),
                                new Coordinat(1,4),
                                new Coordinat(4,4),
                                new Coordinat(1,3),
                                new Coordinat(4,3),
                                new Coordinat(0,2),
                                new Coordinat(2,2),
                                new Coordinat(3,2),
                                new Coordinat(5,2),
                                new Coordinat(1,1),
                                new Coordinat(4,1),
                                new Coordinat(0,0),
                                new Coordinat(0,2),
                                new Coordinat(0,4)
                                ),20)

        );
    }

}