package ru.alexch;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class CountingSortTest {
    @ParameterizedTest
    @MethodSource("getArguments")
    void testCountAndSort(int[] seq,int[] ans){
        CountingSort countingSort = new CountingSort();
       int[]actual =  countingSort.countAndSort(seq);
        Assertions.assertArrayEquals(actual,ans);
    }

    private static Stream<Arguments> getArguments(){
        return Stream.of(Arguments.of(new int[]{5,4,5,4,2,3,1,5,1},new int[]{1,1,2,3,4,4,5,5,5}),
                Arguments.of(new int[]{1,2,3,4,4,4,3,2,1},new int[]{1,1,2,2,3,3,4,4,4}),
                Arguments.of(new int[]{4,3,2,1,0,0,0,0,0},new int[]{0,0,0,0,0,1,2,3,4}),
                Arguments.of(new int[]{0,0},new int[]{0,0}),
                Arguments.of(new int[]{0},new int[]{0})
        );
    }

}