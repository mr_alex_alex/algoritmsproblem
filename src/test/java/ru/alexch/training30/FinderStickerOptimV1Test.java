package ru.alexch.training30;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.TreeSet;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class FinderStickerOptimV1Test {

    @ParameterizedTest
    @MethodSource("getValidationArg")
    void testFindSticker(int n, String seq1, int m, String seq2, Integer[] ans) {
        FinderStickerOptimV1 finder = new FinderStickerOptimV1();
        Integer[] actual = finder.findSticker(n, seq1, m, seq2);

        assertArrayEquals(ans, actual);
    }

    static Stream<Arguments> getValidationArg() {
        return Stream.of(
                Arguments.of(1, "5", 2, "4 6", new Integer[]{0, 1}),
                Arguments.of(3, "100 1 50", 3, "300 0 75", new Integer[]{3, 0, 2}),
                Arguments.of(1, "0", 3, "300 0 75", new Integer[]{1, 0, 1}),
                Arguments.of(4, "3 3 2 2", 4, "3 3 4 2", new Integer[]{1, 1, 2, 0}),
                Arguments.of(1, "179", 1, "179", new Integer[]{0}),
                Arguments.of(30, "1 30 2 29 3 28 4 27 5 26 6 25 7 24 8 23 9 22 10 21 11 20 12 19 13 18 14 17 15 16", 30, "1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31", new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30})
        );
    }

    @ParameterizedTest
    @MethodSource("getValidationArg2")
    void binsearch(Integer[] ar, int num, int ans) {
        FinderStickerOptimV1 finder = new FinderStickerOptimV1();
        TreeSet list = new TreeSet();

        int actual = finder.binsearchRight(ar , num);
        assertEquals(ans, actual);
    }

    static Stream<Arguments> getValidationArg2() {
        return Stream.of(
                Arguments.of(new Integer[]{1, 50, 100}, 300, 2),
                Arguments.of(new Integer[]{1, 50, 100}, 0, -1),
                Arguments.of(new Integer[]{1, 50, 100}, 75, 1)

        );
    }


    @ParameterizedTest
    @MethodSource("getValidationArg3")
    void quickSorter(Integer[] seq, Integer[] ans) {
        FinderStickerOptimV1 sorter = new FinderStickerOptimV1();
        Integer[] actual = sorter.quickSort(seq);
        assertArrayEquals(ans, actual);
    }

    static Stream<Arguments> getValidationArg3() {
        return Stream.of(
                Arguments.of(new Integer[]{100, 50, 10},new Integer[]{10, 50, 100})
        );
    }
}