package ru.alexch;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class GistogrammOfCharsInStringTest {

    @ParameterizedTest
    @MethodSource("getValidationArguments")
     void testPrintChars(String str){
        GistogrammOfCharsInString gistogramm = new GistogrammOfCharsInString();
        gistogramm.printChars(str);
    }
    static Stream<Arguments> getValidationArguments(){
        return Stream.of(
                Arguments.of("Twas brillig, and the slithy toves Did gyre and gimble in the wabe;" +
                        "All mimsy were the borogoves," +
                        "And the mome raths outgrabe."),
                Arguments.of("«.», «!», «?», «:», «-», «,», «;», «(», «)»,Hello, world!"),
                Arguments.of("Hello, world!"),
                Arguments.of("    \n\n   \n\n  !")
        );
    }
}