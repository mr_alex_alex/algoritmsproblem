package ru.alexch;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FinderMaxNumberTest {

    @Test
    void testNormalElement(){
        FinderMaxNumber finderMaxNumber = new FinderMaxNumber();
        int expected = 7;
        var result = finderMaxNumber.finderMax(new int[]{1,2,1,7,3,1});
        assertEquals(expected,result);
    }
    @Test
    void testFirstElement(){
        FinderMaxNumber finderMaxNumber = new FinderMaxNumber();
        int expected = 9;
        var result = finderMaxNumber.finderMax(new int[]{9,2,1,7,3,1});
        assertEquals(expected,result);
    }
    @Test
    void testLastElement(){
        FinderMaxNumber finderMaxNumber = new FinderMaxNumber();
        int expected = 6;
        var result = finderMaxNumber.finderMax(new int[]{5,2,1,4,3,6});
        assertEquals(expected,result);
    }

}
