package ru.alexch;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class FinderTwoMaxNumberTest {

    public <T extends Comparable<T>> boolean isEquals(List<T> list1, List<T> list2) {
        if ((list1 == null) && (list2 == null)) {
            return true;
        } else if ((list1 == null) || (list2 == null)) {
            return false;
        } else if (list1.size() !=  list2.size()) {
            return false;
        }
        list1 = new ArrayList<T>(list1);
        list2 = new ArrayList<T>(list2);

        Collections.sort(list1);
        Collections.sort(list2);

        return list1.equals(list2);
    }

    @Test
    void testNormal() {
        var finder = new FinderTwoMaxNumber();
        List<Integer> expectedNumbers = new ArrayList<>(Arrays.asList(6, 5));
        List<Integer> actualNumbers = finder.findTwoMaxNumber(new int[]{3, 1, 2, 6, 1, 5});
        assertTrue(isEquals(expectedNumbers, actualNumbers));
    }
    @Test
    void testFirstElements() {
        var finder = new FinderTwoMaxNumber();
        List<Integer> expectedNumbers = new ArrayList<>(Arrays.asList(7, 6));
        List<Integer> actualNumbers = finder.findTwoMaxNumber(new int[]{6, 7, 2, 6, 1, 5});
        assertTrue(isEquals(expectedNumbers, actualNumbers));
    }
    @Test
    void testLastElements() {
        var finder = new FinderTwoMaxNumber();
        List<Integer> expectedNumbers = new ArrayList<>(Arrays.asList(6, 9));
        List<Integer> actualNumbers = finder.findTwoMaxNumber(new int[]{2, 1, 2, 5, 9, 6});
        assertTrue(isEquals(expectedNumbers, actualNumbers));
    }
    @Test
    void teststEqualElements() {
        var finder = new FinderTwoMaxNumber();
        List<Integer> expectedNumbers = new ArrayList<>(Arrays.asList(9, 9));
        List<Integer> actualNumbers = finder.findTwoMaxNumber(new int[]{9, 1, 2, 5, 9, 6});
        assertTrue(isEquals(expectedNumbers, actualNumbers));
    }
    @Test
    void teststOnlyTwoElements() {
        var finder = new FinderTwoMaxNumber();
        List<Integer> expectedNumbers = new ArrayList<>(Arrays.asList(9,9));
        List<Integer> actualNumbers = finder.findTwoMaxNumber(new int[]{9,9});
        assertTrue(isEquals(expectedNumbers, actualNumbers));
    }
    @Test
    void testExpectedException(){
        var finder = new FinderTwoMaxNumber();
        ArrayIndexOutOfBoundsException thrown = assertThrows(ArrayIndexOutOfBoundsException.class,
                () ->finder.findTwoMaxNumber(new int[]{9}),"Последовательность меньше заданной");
       assertEquals(finder.findTwoMaxNumber(new int[]{9}),thrown.getMessage());
    }
}