package ru.alexch;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class FinderMisspelledWordInMapTest {
    @ParameterizedTest
    @MethodSource("getArguments")
    public void testText(List dict, List text,List expect){

        FinderMisspelledWordInMap finder = new FinderMisspelledWordInMap();

        List actual = finder.findWord(dict,text);
        Assertions.assertEquals(expect,actual);

        }


    private static Stream<Arguments>  getArguments(){
        return Stream.of(
                Arguments.of(Arrays.asList("azsx","dcfv","fvgb"), Arrays.asList("asx","dc","vgb"),Arrays.asList("asx","vgb")),
                Arguments.of(Arrays.asList("azsx","dcfv","fvgb"), Arrays.asList("axx","dfv","fvgb"),Arrays.asList("dfv","fvgb"))

        );
    }
}