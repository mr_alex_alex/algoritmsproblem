package ru.alexch;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FinderMinEvenNumberTest {

    @Test
    void testNormalNumber() {
       FinderMinEvenNumber finder = new FinderMinEvenNumber();
         int actual = finder.findMinEvenNumber(new int[]{3, 1, 2, 6, 1, 5});
         int expected = 2;
         assertEquals(expected,actual);
    }
    @Test
    void testFirstNumber() {
        FinderMinEvenNumber finder = new FinderMinEvenNumber();
        int actual = finder.findMinEvenNumber(new int[]{2, 1, 6, 4, 1, 5});
        int expected = 2;
        assertEquals(expected,actual);
    }
    @Test
    void testLastNumber() {
        FinderMinEvenNumber finder = new FinderMinEvenNumber();
        int actual = finder.findMinEvenNumber(new int[]{-67, 1, 6, 4, 1, 2});
        int expected = 2;
        assertEquals(expected,actual);
    }
    @Test
    void testNoOneNumber() {
        FinderMinEvenNumber finder = new FinderMinEvenNumber();
        int actual = finder.findMinEvenNumber(new int[]{5,3,7,9,1,11});
        int expected = -1;
        assertEquals(expected,actual);
    }
    @Test
    void testOneNumber() {
        FinderMinEvenNumber finder = new FinderMinEvenNumber();
        int actual = finder.findMinEvenNumber(new int[]{-8});
        int expected = -8;
        assertEquals(expected,actual);
    }
    @Test
    void testNegativeNumber() {
        FinderMinEvenNumber finder = new FinderMinEvenNumber();
        int actual = finder.findMinEvenNumber(new int[]{-1,3,-7,-4,-2});
        int expected = -4;
        assertEquals(expected,actual);
    }
}