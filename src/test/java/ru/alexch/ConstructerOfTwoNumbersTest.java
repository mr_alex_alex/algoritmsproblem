package ru.alexch;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ConstructerOfTwoNumbersTest {

    @ParameterizedTest
    @MethodSource("getArgumentsEq")
    void ifEqualsTwoSequenceOfDigit(int seq1, int seq2, boolean isEquals) {
        ConstructerOfTwoNumbers constructer = new ConstructerOfTwoNumbers();
        boolean actual = constructer.IfEqualsTwoSequenceOfDigit(seq1,seq2);
        assertEquals(isEquals,actual);
    }

     static Stream<Arguments> getArgumentsEq(){
        return Stream.of(Arguments.of(2021,2012,true),
                Arguments.of(123456789,987654321,true),
                Arguments.of(103050709,135790000,true),
                Arguments.of(123456781,231147658,true),
                Arguments.of(223456722,222234567,true),
                Arguments.of(10090,90010,true),
                Arguments.of(10090,18,false)

        );
    }

    @ParameterizedTest
    @MethodSource("getArguments")
    void toConstructSeq(int seq,int[] expected) {
        ConstructerOfTwoNumbers constructer = new ConstructerOfTwoNumbers();
        int[] actual = constructer.toConstructSeq(seq);

        Assertions.assertArrayEquals(expected,actual);
    }

    static Stream<Arguments> getArguments(){
        return Stream.of(Arguments.of(2021,new int[]{1,1,2,0,0,0,0,0,0,0}),
                Arguments.of(123456789,new int[]{0,1,1,1,1,1,1,1,1,1}),
                Arguments.of(103050709,new int[]{4,1,0,1,0,1,0,1,0,1}),
                Arguments.of(123456781,new int[]{0,2,1,1,1,1,1,1,1,0}),
                Arguments.of(223456722,new int[]{0,0,4,1,1,1,1,1,0,0}),
                Arguments.of(10090,new int[]{3,1,0,0,0,0,0,0,0,1})
        );
    }
}