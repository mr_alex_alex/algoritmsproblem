package ru.alexch;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class FinderZeroinRangeTest {
    @ParameterizedTest
    @MethodSource("getVerificationArguments")
    public void testCountZeros(String str, int L, int R, int ans) throws Exception {
        FinderZeroinRange finder = new FinderZeroinRange();
        int[] actualPrefix = FinderZeroinRange.creater(str);
        int actual = FinderZeroinRange.countzeros(actualPrefix, L, R);
        int expected = ans;
        Assertions.assertEquals(expected,actual);
    }

    @Test
    void shouldThrowExceptionIfStringEmpty(){
        String seq  = "  ";
        assertThrows(NumberFormatException.class ,()->FinderZeroinRange.creater(seq));
    }


    static Stream<Arguments> getVerificationArguments() {
        return Stream.of(
               //                       1 2 3 4 5 6 7 8 9 11
                Arguments.of("1 5 3 0 6 8 0 0 9 1 7 ", 3, 8, 2),
                Arguments.of("1 5 3 0 6 8 0 0 9 1 7 ", 1, 11, 3),
                Arguments.of("1 5 3 0 6 8 0 0 9 1 7 ", 1, 4, 0),
                Arguments.of("1 5 3 0 6 8 0 0 9 1 7 ", 4, 8, 2),
                Arguments.of("0 0 0 0", 1, 4, 3),
                Arguments.of("  0 0   ", 1, 2, 1)



        );
    }

}