package ru.alexch;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

class FinderNumberWaterCubeTest {

    @Test
    void testFindNormalSequence() {
    FinderNumberWaterCube finder = new FinderNumberWaterCube();
    String[] array = new String[]{"nnn","n","nnnn","nnn","nnnnn","n","n","nnn","n"};
    int actual = finder.findNumberWaterCube(array);
    int expected = 7;
    assertEquals(expected, actual);

    }

    @Test
    void testCountRockCubeInPillar() {
        FinderNumberWaterCube finder = new FinderNumberWaterCube();
        int actual = finder.countRockCubeInPillar("nnn");
        int expected = 3;
        assertEquals(expected,actual);
    }

    @Test
    void findWaterCubeFromSeqLeft() {
        FinderNumberWaterCube finder = new FinderNumberWaterCube();
        int actual = finder.findWaterCubeFromSeqLeft(new int[]{1,3,1,2,1,3,1,1,2,4},9,1);
        int expected = 10;
        assertEquals(expected,actual);

    }

    @Test
    void testFindWaterCubeFromSeqRight() {
        FinderNumberWaterCube finder = new FinderNumberWaterCube();
        int actual = finder.findWaterCubeFromSeqRight(new int[]{4,1,2},0,2);
        int expected = 1;
        assertEquals(expected,actual);
    }

    @Test
    void testCountOfRockCubesInSeq() {
        FinderNumberWaterCube finder = new FinderNumberWaterCube();
        int[] array= new int[]{1,3,1,2,1,3,1,1,2,4,1,2};
        int actual = finder.CountOfRockCubesInSeq(array,0,array.length-1);
        int expended = 22;
        assertEquals(expended,actual);
    }
}