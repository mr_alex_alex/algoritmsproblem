package ru.alexch;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class GroupingWordBySharesCharactersTest {

    @ParameterizedTest
    @MethodSource("getValidationArguments")
    void testGroupWords(List list) {
        GroupingWordBySharesCharacters grouping = new GroupingWordBySharesCharacters();
        Map<Integer, List<String>> actual = grouping.groupWords(list);
        Map<Integer, List<String>> expected =new TreeMap<>(Collections.reverseOrder());
        expected.put(1,Arrays.asList("bat"));
        expected.put(2,Arrays.asList("nat","tan"));
        expected.put(3,Arrays.asList("ate","eat","tea"));
        assertEquals(expected,actual);
    }

    static Stream<Arguments> getValidationArguments() {
        return Stream.of(
                Arguments.of(Arrays.asList("eat", "tea", "tan", "ate", "nat", "bat"))
        );
    }

}