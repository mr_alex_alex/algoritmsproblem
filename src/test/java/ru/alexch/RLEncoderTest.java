package ru.alexch;

import com.sun.jdi.connect.Connector;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class RLEncoderTest {

   private final RLEncoder encoder = new RLEncoder();

   @ParameterizedTest
   @MethodSource("getValidationArguments")
    public void testEncode(String s,String d ){
      String  actuals = encoder.encode(s);
       assertEquals( d,actuals);
    }


    static Stream<Arguments> getValidationArguments(){
        return Stream.of(
                Arguments.of("AAAABBBCC","A4B3C2"),
                Arguments.of("SSSERTDDD","S3ERTD3")
        );
    }

}