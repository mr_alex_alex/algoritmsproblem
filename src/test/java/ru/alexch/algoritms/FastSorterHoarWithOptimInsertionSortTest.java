package ru.alexch.algoritms;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import ru.alexch.training30.FinderStickerOptimV1;

import java.io.*;
import java.util.Locale;
import java.util.stream.Stream;

import static java.lang.Integer.parseInt;
import static org.junit.jupiter.api.Assertions.*;

class FastSorterHoarWithOptimInsertionSortTest {
    @Test
    void testQuickSort() throws IOException {
        Locale.setDefault(Locale.US);
        long startTime = System.currentTimeMillis();
        String fileName = "./Testinput.txt";
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        String strOfDiegoNomers;
        StringBuilder sb1 = new StringBuilder("");
        if ((strOfDiegoNomers = reader.readLine()) != null) {
            sb1.append(strOfDiegoNomers);
        }
        reader.close();

        Integer[] numsDiego = Stream.of(strOfDiegoNomers.trim().split(" "))
                .map(Integer::parseInt)
                .toArray(Integer[]::new);
        Integer[] result = FastSorterHoarWithOptimInsertionSort.quickSort(numsDiego);
        OutputStream outputStream = new FileOutputStream("Testoutput.txt");
        PrintWriter printWriter = new PrintWriter(outputStream);
        for (Integer item :result
        ) {
            printWriter.println(item);
        }
        printWriter.close();
        outputStream.close();
        long endTime = System.currentTimeMillis();
        System.out.println(result.length  + " ms "+ (endTime-startTime));
   //     assertArrayEquals(ans, result);
    }

//    static Stream<Arguments> getValidationArguments1() {
//        return Stream.of(
//                Arguments.of(new Integer[]{1, 2, 1, 0, 1, 4, 8, 4, 2, 0}, new Integer[]{0, 0, 1, 1, 1, 2, 2, 4, 4, 8}),
//                Arguments.of(new Integer[]{100, 50, 1}, new Integer[]{1, 50, 100}),
//                Arguments.of(new Integer[]{50, 1, 100}, new Integer[]{1, 50, 100})
//
//        );
//    }


}