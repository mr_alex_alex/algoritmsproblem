package ru.alexch.algoritms;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class FastSorterWithHoarPartitionTest {
    @ParameterizedTest
    @MethodSource("getValidationArguments1")
    void testQuickSort(int[] array,int[] ans) {

        assertArrayEquals(ans,FastSorterWithHoarPartition.quickSort(array));
    }

    static Stream<Arguments> getValidationArguments1() {
        return Stream.of(
                Arguments.of(new int[]{1, 2, 1, 0, 1, 4,8,4,2,0}, new int[]{0,0,1,1,1,2,2,4,4,8}),
                Arguments.of(new int[]{100,50,1}, new int[]{1,50,100}),
                Arguments.of(new int[]{50,1,100}, new int[]{1,50,100})

        );
    }

}