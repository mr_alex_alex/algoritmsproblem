package ru.alexch;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FinderTheShortestWordsTest {

    @Test
    void testNormalElement() {
        String[] testdata = {"aa","bbb","cc","dddd"};
        FinderTheShortestWords finder = new FinderTheShortestWords();
        String expected = "aa cc";
        String actual =  finder.findTheShortestWords(testdata);
        assertEquals(expected,actual);
    }
    @Test
    void testOneElement() {
        String[] testdata = {"dddd"};
        FinderTheShortestWords finder = new FinderTheShortestWords();
        String expected = "dddd";
        String actual =  finder.findTheShortestWords(testdata);
        assertEquals(expected,actual);
    }
    @Test
    void testFirstElement() {
        String[] testdata = {"aa","bbbb","cccc","ddddd"};
        FinderTheShortestWords finder = new FinderTheShortestWords();
        String expected = "aa";
        String actual =  finder.findTheShortestWords(testdata);
        assertEquals(expected,actual);
    }
    @Test
    void testLastElement() {
        String[] testdata = {"aaddd","bbbb","cccc","ddd"};
        FinderTheShortestWords finder = new FinderTheShortestWords();
        String expected = "ddd";
        String actual =  finder.findTheShortestWords(testdata);
        assertEquals(expected,actual);
    }
    @Test
    void testEmptyElement() {
        String[] testdata = {" "};
        FinderTheShortestWords finder = new FinderTheShortestWords();
        String expected = "";
        String actual =  finder.findTheShortestWords(testdata);
        assertEquals(expected,actual);
    }
    @Test
    void testNumberAsStringElement() {
        String[] testdata = {"1134","bb34","333","ddddd#$"};
        FinderTheShortestWords finder = new FinderTheShortestWords();
        String expected = "333";
        String actual =  finder.findTheShortestWords(testdata);
        assertEquals(expected,actual);
    }
}