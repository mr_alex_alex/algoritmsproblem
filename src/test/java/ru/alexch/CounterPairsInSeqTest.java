package ru.alexch;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Map;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class CounterPairsInSeqTest {

    @ParameterizedTest
    @MethodSource("getVerificationArguments")
    public void testCountZeros(int[] seq,  int ans) {

        Map actualPrefix = CounterPairsInSeq.countPrefixSum(seq);
        int actual = CounterPairsInSeq.countZerosSumRangers(actualPrefix);
        int expected = ans;
        Assertions.assertEquals(expected,actual);
    }

    static Stream<Arguments> getVerificationArguments() {
        return Stream.of(
                //                       1 2 3 4 5 6 7 8 9 11
                Arguments.of(new int[]{2,-2}, 1 ),
                Arguments.of(new int[]{2,-2,3,-3}, 2 )
        );
    }

}