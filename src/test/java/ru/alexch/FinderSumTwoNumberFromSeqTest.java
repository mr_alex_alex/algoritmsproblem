package ru.alexch;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

class FinderSumTwoNumberFromSeqTest {


    @ParameterizedTest
    @MethodSource("getArguments")
    void testFindSum(int[] seq, int X, int[] ans){
        FinderSumTwoNumberFromSeq finder = new FinderSumTwoNumberFromSeq();
      int[] expected = finder.findPair(seq,X);
        Assertions.assertArrayEquals(expected,ans);
    }

    private static Stream<Arguments> getArguments(){
        return Stream.of(
                Arguments.of(new int[]{1,2,3,4,5,6,7},5, new int[]{2,3}),
                Arguments.of(new int[]{2,4,5,7,10},3, new int[]{0,0}),
                Arguments.of(new int[]{2,4,5,7,10},3, new int[]{0,0}),
                Arguments.of(new int[]{2,2,2,2,2,2,4},4, new int[]{0,0})
        );
    }

}